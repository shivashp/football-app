<?php include('header.php'); ?>

<section class="content login-register" id="register">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
        <div class="box">
          <div class="header">
            Register
          </div><!-- header -->
          <div class="body">
            <form  name="user-register">
              <label>Picture:</label>
              <input type="file" id="photo" name="photo" style="margin-bottom:20px">
              <label>Name:</label>
              <input type="text" id="name" name="name" placeholder="Name" class="form-control" required>
              <label>Age:</label>
              <input type="text" id="age" name="age" placeholder="Age" class="form-control" required>
              <label>Address:</label>
              <input type="text" id="address" name="address" placeholder="Address" class="form-control" required>
              <label>Email:</label>
              <input type="text" id="email" name="email" placeholder="Email" class="form-control" required>
              <label>Password:</label>
              <input type="password" id="password" name="password" placeholder="Password" class="form-control" required>
              <div>
                <input type="checkbox" id="policy"> I agree to the <a href="#">privacy policy</a> and <a href="#">terms and conditions</a>
              </div>
              <div class="btn-section">
                <input type="submit" name="" id="save" class="btn btn-primary" value="Create new Account">
              </div>
              <div class="account-msg">
                Already have an account? <a href="login.php">Log in</a>
              </div>
            </form>
          </div><!-- body -->
        </div><!-- box -->
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- login -->

<?php include('footer.php'); ?>
<script src="scripts/register.js" charset="utf-8"></script>
