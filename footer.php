<footer>
  <div class="container">
    <div id="sponsors">
      <div class="title">SPONSORS</div>
      <div class="companies">
        <img src="img/companies/partner1.png">
        <img src="img/companies/partner2.png">
        <img src="img/companies/partner3.png">
        <img src="img/companies/partner4.png">
        <img src="img/companies/partner5.png">
        <img src="img/companies/partner6.png">
      </div>
    </div><!-- sponsors -->
    <div id="social-media">
      <div class="title">Follow us on Social media</div>
      <div class="media-icons">
        <a href="javascript:;"><img src="img/icons/facebook.png"></a>
        <a href="javascript:;"><img src="img/icons/instagram.png"></a>
        <a href="javascript:;"><img src="img/icons/youtube.png"></a>
      </div>
    </div>
  </div><!-- container -->
  <div id="bottom-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-xs-12">
          <ul>
            <li><a href="about.php">About us</a></li>
            <li><a href="javascript:;">Terms of Use</a></li>
            <li><a href="javascript:;">Privacy Policy</a></li>
            <li><a href="tickets.php">Tickets</a></li>
            <li><a href="fixture.php">Fixture</a></li>
            <li><a href="result.php">Result</a></li>
            <li><a href="contact.php">Contact Us</a></li>
          </ul>
        </div>
        <div class="col-md-5 col-xs-12">
          <div class="copyright pull-right">Copyright &copy; 2017. Greenacre Eagles. All Rights Reserved</div>
        </div>
      </div><!-- row -->
    </div><!-- container -->
  </div><!-- bottom-bar -->
</footer>

</body>
<!-- <script src="js/jquery.min.js" charset="utf-8"></script> -->
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js" charset="utf-8"></script>
<script src="admin-panel/js/moment.min.js" charset="utf-8"></script>
<script src="admin-panel/js/bootstrap-notify.js" charset="utf-8"></script>
<script src="js/config.js" charset="utf-8"></script>
<script src="js/commonfunctions.js" charset="utf-8"></script>
</html>
