<?php include('header.php'); ?>

<section class="content login-register" id="login">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
        <div class="box">
          <div class="header">
            Login
          </div><!-- header -->
          <div class="body">
            <form>
              <label>Email:</label>
              <input type="text" id="email" placeholder="Email" class="form-control">
              <label>Password:</label>
              <input type="password" id="password" placeholder="Password" class="form-control">
              <div>
                <!-- <a class="forgot-pwd" href="forgot-password.php">Forgot password?</a> -->
              </div>
              <div class="btn-section">
                <button type="submit" class="btn btn-primary" id="login-user" name="button">Login</button>
              </div>
              <div class="account-msg">
                Don't have an account? <a href="register.php">Sign Up</a>
              </div>
            </form>
          </div><!-- body -->
        </div><!-- box -->
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- login -->

<?php include('footer.php'); ?>
<script src="scripts/login.js" charset="utf-8"></script>
