<?php include('header.php'); ?>

<div class="content">
  <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12">

      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Add News</h4>
        </div><!-- card-header-->
        <div class="card-content">
          <form name="add-news-form">
            <input type="text" name="title" class="form-control" placeholder="Title" id="title">
            <textarea id="desc" name="desc" class="form-control" placeholder="Content" rows="8" cols="80"></textarea>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
            <button type="submit" id="save" class="btn btn-success pull-right">Save</button>
        </div><!-- card-content-->
      </div><!-- card-->

    </div><!-- col-sm-8 left-side -->
    <div class="col-md-4 col-sm-4 col-xs-12">

      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Featured Image</h4>
        </div><!-- card-header-->
        <div class="card-content text-center">

          <div class="fileinput fileinput-new text-center" data-provides="fileinput">
              <div class="fileinput-new thumbnail">
                  <img src="" class="preview-image" alt="">
              </div>
              <div>
                  <span class="btn btn-rose btn-round btn-file">
                      <span class="fileinput-new">Select image</span>
                      <input type="file" id="featured" name="featured" />
                  </span>
                  </form>
              </div>
          </div><!-- fileinput -->

        </div><!-- card-content-->
      </div><!-- card-->

    </div><!-- col-sm-8 left-side -->

  </div><!-- row-->
</div><!-- content -->

<?php include('footer.php'); ?>
<script src="ckeditor/ckeditor.js"></script>
<script src="scripts/add-news.js" charset="utf-8"></script>
<script>
  active('news');
  CKEDITOR.replace('desc');
</script>
<style media="screen">
  .fileinput .thumbnail {
    max-width: 100%;
  }
</style>
