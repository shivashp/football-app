<?php  include('header.php'); ?>

<div class="content">
  <div class="card">
      <div class="card-header card-header-icon" data-background-color="purple">
          <i class="material-icons">person</i>
      </div>
      <div class="card-content">
          <h4 class="card-title">Add Player</h4>
          <form class="form-horizontal" id="add-player" name="add-player">

            <div class="row add-emp-row">
              <div class="col-md-3">
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        <img src="" id="preview-image1" alt="">
                    </div>
                    <div>
                        <span class="btn btn-rose btn-round btn-file">
                            <span class="fileinput-new">Select Main image</span>
                            <input type="file" id="image" class="image-select" data-id="1" name="image" />
                        </span>
                        </form>
                    </div>
                </div><!-- fileinput -->
              </div><!-- col-md-4-->

              <div class="col-md-9 text-center">
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        <img src="" id="preview-image2" alt="">
                    </div>
                    <div>
                        <span class="btn btn-rose btn-round btn-file">
                            <span class="fileinput-new">Select Featured image</span>
                            <input type="file" id="featured" class="image-select" data-id="2" name="featured" />
                        </span>
                        </form>
                    </div>
                </div><!-- fileinput -->
              </div><!-- col-md-4-->

            </div><!--row -->

            <div class="row add-emp-row">

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label">Team</label>
                    <select name="team_id" id="team_id" data-style="select-with-transition" title="Choose Team" data-size="7">
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                  <label class="control-label">First Name</label>
                  <input type="text" name="first_name" id="first_name" class="form-control" value="">
                  <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                    <label class="control-label">Last Name</label>
                    <input type="text" name="last_name" id="last_name" class="form-control">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

            </div><!--row -->

            <div class="row add-emp-row">
              <div class="col-md-4">
                <div class="form-group label-floating">
                    <label class="control-label">Squad No.</label>
                    <input type="number" name="squad_no" class="form-control" id="squad_no" value="">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label">Position</label>
                    <select class="selectpicker" name="position" id="position" data-style="select-with-transition" title="Choose Position" data-size="7">
                      <option value="goalkeeper">Goalkeeper</option>
                      <option value="defender">Defender</option>
                      <option value="midfielder">Midfielder</option>
                      <option value="forward">Forward</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group" style="margin-top:-6px;">
                    <label class="control-label date-label">Date of Birth</label>
                    <input type="text" name="dob" id="dob" class="datepicker form-control" >
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->
            </div><!--row -->

            <div class="row add-emp-row">
              <div class="col-md-12">
                <div class="form-group label-floating">
                  <label class="control-label">About Player</label>
                  <textarea name="desc" id="desc" class="form-control" rows="8" cols="80"></textarea>
                  <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->
            </div><!--row -->

            <div class="row text-center" style="margin-top:20px;">
              <button type="submit" name="Save" class="btn btn-success" id="add">Save</button>
              <input type="reset" class="btn btn-danger" name="Clear">
            </div>
          </form>
      </div> <!-- end content-->
  </div> <!--  end card  -->
</div><!-- content -->
<?php include('footer.php'); ?>
<script>
  $().ready(function() {
      demo.initFormExtendedDatetimepickers();
  });
</script>
<script src="scripts/add-player.js" charset="utf-8"></script>
<style>
  .btn-group.bootstrap-select {
    margin-top: 2px;
  }
  label.error {
    color: #f44336;
  }
  label.control-label.date-label {
    font-size: 14px;
    margin-top: -27px;
    margin-bottom: 8px;
}
.fileinput .thumbnail {
 max-width: 100%;
}
</style>
