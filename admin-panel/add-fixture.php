<?php  include('header.php'); ?>
<style>
  .btn-group.bootstrap-select {
    margin-top: 2px;
  }
  label.error {
    color: #f44336;
  }
  label.control-label.date-label {
    font-size: 14px;
    margin-top: -27px;
    margin-bottom: 8px;
}
.fileinput .thumbnail {
 max-width: 100%;
}
.ticket-details {
  display: none;
}
</style>
<div class="content">
  <div class="card">
      <div class="card-header card-header-icon" data-background-color="purple">
          <i class="material-icons">person</i>
      </div>
      <div class="card-content">
          <h4 class="card-title">Add Fixture</h4>
          <form class="form-horizontal" id="add-fixture" name="add-fixture">

            <div class="row add-emp-row">

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label">Team A</label>
                    <select name="team_id" class="team_list" id="teamA_id" data-style="select-with-transition" title="Choose Team A" data-size="7">
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label">Team B</label>
                    <select name="team_id" class="team_list" id="teamB_id" data-style="select-with-transition" title="Choose Team B" data-size="7">
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group" style="margin-top:-6px;">
                    <label class="control-label date-label">Match Date</label>
                    <input type="text" name="dob" id="date" class="datepicker form-control" >
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

            </div><!--row -->

            <div class="row add-emp-row">
              <div class="col-md-4">
                <div class="form-group" style="margin-top:-6px;">
                    <label class="control-label date-label">Match Time</label>
                    <input type="text" id="time" class="form-control timepicker form-control" >
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                    <label class="control-label">Location</label>
                    <input type="text" name="location" class="form-control" id="location" value="">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label">Tickets</label>
                    <select class="selectpicker" name="position" id="tickets_enabled" data-style="select-with-transition" title="Enable/Disable Tickets" data-size="7">
                      <option value="true">Allow Tickets</option>
                      <option value="false">Dont Allow Tickets</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

            </div><!--row -->

            <div class="row">
              <div class="col-md-4 ticket-details">
                <div class="form-group label-floating">
                    <label class="control-label">Ticket Price</label>
                    <input type="text" name="location" class="form-control" id="price" value="">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <!-- <div class="col-md-4 ticket-details">
                <div class="form-group label-floating">
                    <label class="control-label">Total Available Tickets</label>
                    <input type="number" name="location" class="form-control" id="tickets_count" value="">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!- col-md-4-> -->
            </div><!--row -->

            <div class="row text-center" style="margin-top:20px;">
              <button type="submit" name="Save" class="btn btn-success" id="add">Save</button>
              <input type="reset" class="btn btn-danger" name="Clear">
            </div>
          </form>
      </div> <!-- end content-->
  </div> <!--  end card  -->
</div><!-- content -->
<?php include('footer.php'); ?>
<script>
  $().ready(function() {
      demo.initFormExtendedDatetimepickers();
  });
</script>
<script src="scripts/add-fixture.js" charset="utf-8"></script>
