<!Doctype html>
<html lang="en" class = "per1 per2">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>HRIS Admin</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/demo.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link rel="stylesheet" type="text/css" href="css/material-icons.css" />
    <link rel="stylesheet" href="css/main.css">
</head>
<script>
  var access_token = localStorage.getItem('admin_access_token');
  if(access_token == null || access_token == undefined || access_token == ''){
    window.location.href="index.html";
  }
</script>
<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="img/sidebar-1.jpg">
          <div class="logo"> <a href="javascript:;" class="simple-text">
              GreenAcre
          </div>
          <div class="logo logo-mini"> <a href="#" class="simple-text"> GA </a> </div>
          <div class="sidebar-wrapper">
              <ul class="nav" id="main">
                  <li class="link news"><a href="news.php"><i class="material-icons">library_books</i> <p>News </p></a></li>
                  <li class="link teams"><a href="teams.php"><i class="material-icons">contacts</i> <p>Teams </p></a></li>
                  <li class="link players"><a href="players.php"><i class="material-icons">peoples</i> <p>Players </p></a></li>
                  <li class="link fixtures"><a href="fixtures.php"><i class="material-icons">event_note</i> <p>Fixtures </p></a></li>
              </ul>
          </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-blue navbar-absolute">
                <div class="container-fluid">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                  </div>
                    <div class="collapse navbar-collapse">
                      <ul class="nav navbar-nav navbar-right">
                          <li class="dropdown">
                              <a href="dashboard.html#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">account_circle</i> ADMIN <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li>
                                  <a href="change-password.php">Change Password</a>
                                </li>
                                <li style="cursor:pointer">
                                  <a href="logout.php" class="logout">Logout</a>
                                </li>
                              </ul>
                          </li>
                          <li class="separator hidden-lg hidden-md"></li>
                      </ul>
                    </div>
                </div>
            </nav>
<style>
.navbar-nav>li>.dropdown-menu {
  margin-top: 10px;
}
</style>
