$(function() {

FUNC.getSingleFixture(id, function(err, data) {
  if(!err) {
    var data = data.data;
    var teamA = data.teamA;
    var teamB = data.teamB;
    $("#teamA_label").html(teamA + " Score");
    $("#teamB_label").html(teamB + " Score");
  }
});

$("#add").click(function(e) {
  e.preventDefault();
  var score1 = $("#teamA_score").val();
  var score2 = $("#teamB_score").val();
  var arr = [score1.isBlank("Score"), score2.isBlank("Score")];
  if(checkEmpty(arr)){
    return false;
  }
  FUNC.updateResult({
    fixture_id: id,
    teamA_score: score1,
    teamB_score: score2,
    access_token
  }, function(err, data) {
    if(!err) {
      showSuccess("Result Updated Successfully!");
      setTimeout(function () {
        window.location.href="fixtures.php";
      }, 2000);
    }
  })
})



})
