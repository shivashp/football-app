$(function() {

  $(".image-select").change(function() {
    var id = $(this).attr("data-id");
    readURL(this, "#preview-image"+id);
  })

  FUNC.getAllTeams(function(err, data) {
    if(!err) {
      var team = prepare_selectpicker(data.data);
      $("#team_id").html(team);
      $('#team_id').selectpicker({
        size: 7,
      });
    }
  }); // get all team

  $("#add").click(function(e) {
    e.preventDefault();
    var team_id = $("#team_id").val();
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var squad_no = $("#squad_no").val();
    var position = $("#position").val();
    var dob = $("#dob").val();
    var desc = $("#desc").val();
    var image = $("#image").val();
    var featured = $("#featured").val();
    var age = _calculateAge(new Date(dob));
    var arr = [image.isBlank("Main Image"), featured.isBlank("Featured Image"), team_id.isBlank("Team"), first_name.isBlank("First Name"), last_name.isBlank("Last Name"), squad_no.isBlank("Squad No"), position.isBlank("Position"), dob.isBlank("Date of Birth"), desc.isBlank("Player Description")];
    if(checkEmpty(arr)){
      return false;
    }    
    var formData = new FormData(document.forms.namedItem("add-player"));
    formData.append("access_token", access_token);
    formData.append("age", age);
    formData.append("team_id", team_id);
    formData.append("first_name", first_name);
    formData.append("last_name", last_name);
    formData.append("squad_no", squad_no);
    formData.append("position", position);
    formData.append("dob", dob);
    formData.append("desc", desc);
    FUNC.addPlayer(formData, function(err, data) {
      if(!err && data !== null) {
        showSuccess("Player Added Successfully!");
        setTimeout(function () {
          window.location.href="players.php";
        }, 2000);
      }
    })
  });

})
