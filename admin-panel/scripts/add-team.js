$(function() {

  $("#logo").change(function() {
    readURL(this, ".preview-image");
  })

  $("#save").click(function(e) {
    e.preventDefault();
    var name = $("#name").val();
    var desc = CKEDITOR.instances['desc'].getData()
    var image = $("#logo").val();
    var arr = [name.isBlank("Title"), desc.isBlank("Description"), image.isBlank("Logo")];
    if(checkEmpty(arr)){
      return false;
    }
    var formData = new FormData(document.forms.namedItem("add-team-form"));
    formData.append("access_token", access_token);
    formData.append("desc", desc);
    FUNC.addTeam(formData, function(err, data) {
      if(!err) {
        showSuccess("Team Added Successfully!");
        setTimeout(function () {
          window.location.href="teams.php";
        }, 2000);
      }
    })

  });

});
