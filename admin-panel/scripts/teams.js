$(function() {

getTeams();

function getTeams() {
  FUNC.getAllTeams(function(err, data) {
    if(!err) {
      $("#data-body").html('');
      if(data.data == '') {
        $("#data-body").html('');
      } else {
        data.data.map(function(team) {
          var str="";
          var desc = (team.team_description.length > 115)?team.team_description.substr(0,115)+'...':team.team_description;
          desc = desc.replace(/<(?:.|\n)*?>/gm, '');
          str += "<tr>";
          str += "                      <td style=\"max-width:200px\"><img src='../"+team.logo+"' class=\"featured-image\"><\/td>";
          str += "                      <td>"+team.name+"<\/td>";
          str += "                      <td>"+desc+"<\/td>";
          str += "                      <td style=\"min-width:180px\" class=\"text-right\"><a class=\"btn btn-danger btn-sm delete\"  data-id=\""+team.team_id+"\"><i class=\"material-icons\">delete<\/i><\/a><\/td>";
          str += "                    <\/tr>";
          $("#data-body").append(str);
        });
      }
      $("#datatables").DataTable();
    }
  });
}

$(document).delegate(".delete", "click", function() {
  if(!confirm("Are you sure you want to delete?")){return false;}
  var id = $(this).attr("data-id");
  FUNC.deleteTeam(id, access_token, function(err, data) {
    if(!err) {
      showSuccess("Team Deleted Successfully!");
      getTeams();
    }
  })
})

})// Document Ready
