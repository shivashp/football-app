function clearAlert() {
  $(".sp-alert-danger").html('');
  $(".sp-alert-danger").hide();
}
function showError(message) {
  $(".sp-alert-danger").html(message);
  $(".sp-alert-danger").show();
}

String.prototype.isBlank = function (type) {
  var value = this.toString();
  value = value.trim();
  if (value == '' || (/^\s*$/.test(value))) {
    showError(type + " is required!")
    return true;
  }
  else
  return false;
};

$(function() {
  $("#login-btn").click(function(e) {
    console.log("Login");
    e.preventDefault();
    var email = $("#email").val();
    var password = $("#password").val();
    if(email.isBlank("Email")) {
      return false;
    }
    if(password.isBlank("Password")) {
      return false;
    }
    clearAlert();
    $.ajax({
        url: basepath + "admin-login",
        type: "POST",
        beforeSend: function() {
          $(".loader").show();
          $("#login-btn").hide();
        },
        data: {
          "email": email,
          "password": password
        },
        success: function(data) {
          if(data.status) {
            var token = data.data.access_token;
            localStorage.clear();
            localStorage.setItem('admin_access_token', token);
            window.location.href = "news.php";
          } else {
            $(".loader").hide();
            $("#login-btn").show();
            showError(data.message);
          }
        },
        error: function(error) {
          $("#login-btn").show();
          showError("Error in Server! Try again!")
        },
    });// Ajax
  })// Login click
});
