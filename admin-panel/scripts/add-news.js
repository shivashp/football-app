$(function() {

  $("#featured").change(function() {
    readURL(this, ".preview-image");
  })

  $("#save").click(function(e) {
    e.preventDefault();
    var title = $("#title").val();
    var desc = CKEDITOR.instances['desc'].getData()
    var image = $("#featured").val();
    var arr = [title.isBlank("Title"), desc.isBlank("Description")];
    if(checkEmpty(arr)){
      return false;
    }
    var formData = new FormData(document.forms.namedItem("add-news-form"));
    formData.append("access_token", access_token);
    formData.append("desc", desc);
    FUNC.addNews(formData, function(err, data) {
      if(!err) {
        showSuccess("News Added Successfully!");
        setTimeout(function () {
          window.location.href="news.php";
        }, 2000);
      } 
    })

  });

});
