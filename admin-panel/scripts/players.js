$(function() {

getPlayers();

function getPlayers() {
  FUNC.getAllPlayers(function(err, data) {
    if(!err && data !== null) {
      $("#data-body").html('');
      if(data.data !== '') {      
        data.data.map(function(player) {
          var str="";
          str += "<tr>";
          str += "                      <td style=\"max-width:200px\"><img src='../"+player.image+"' class=\"featured-image\"><\/td>";
          str += "                      <td>"+player.first_name+" "+player.last_name+"<\/td>";
          str += "                      <td>"+player.name+"<\/td>";
          str += "                      <td>"+player.squad_no+"<\/td>";
          str += "                      <td>"+player.position+"<\/td>";
          str += "                      <td>"+player.age+"<\/td>";
          str += "                      <td style=\"min-width:180px\" class=\"text-right\"><a class=\"btn btn-success btn-sm edit\" data-id=\""+player.player_id+"\"><i class=\"material-icons\">edit<\/i><\/a> <a class=\"btn btn-danger btn-sm delete\"  data-id=\""+player.player_id+"\"><i class=\"material-icons\">delete<\/i><\/a><\/td>";
          str += "                    <\/tr>";
          $("#data-body").append(str);
        });
      }
      $("#datatables").DataTable();
    }
  });
}

$(document).delegate(".delete", "click", function() {
  if(!confirm("Are you sure you want to delete?")){return false;}
  var id = $(this).attr("data-id");
  FUNC.deletePlayer(id, access_token, function(err, data) {
    if(!err) {
      showSuccess("Player Deleted Successfully!");
      getPlayers();
    }
  })
})

})// Document Ready
