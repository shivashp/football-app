$(function() {

$("#update-user").click(function(e) {
  e.preventDefault();
  var oldpassword = $("#old-password").val();
  var newpassword = $("#update-password").val();
  var retype = $("#conf_update_password").val();

  var arr = [oldpassword.isBlank("Old Password"), newpassword.isBlank("New Password"), retype.isBlank("Retype Password")];
  if(checkEmpty(arr)){
    return false;
  }

  if(newpassword !== retype) {
    showError("Passwords don't match");
    return false;
  }


  FUNC.updateAdminPassword({
    oldpassword, newpassword, access_token
  }, function(err, data) {
    if(!err) {
      showSuccess("Password Updated Successfully");
      

    }
  })

})


})// Document
