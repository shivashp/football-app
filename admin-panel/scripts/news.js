$(function() {

getNews();

function getNews() {
  FUNC.getAllNews(function(err, data) {
    if(!err) {
      $("#data-body").html('');
      data.data.map(function(news) {
        var str="";
        var desc = (news.description.length > 115)?news.description.substr(0,115)+'...':news.description;
        desc = desc.replace(/<(?:.|\n)*?>/gm, '');
        str += "<tr>";
        str += "                      <td style=\"max-width:200px\"><img src='../"+news.featured_image+"' class=\"featured-image\"><\/td>";
        str += "                      <td>"+news.title+"<\/td>";
        str += "                      <td>"+desc+"<\/td>";
        str += "                      <td style=\"min-width:180px\" class=\"text-right\"><a class=\"btn btn-success btn-sm edit\" data-id=\""+news.news_id+"\"><i class=\"material-icons\">edit<\/i><\/a> <a class=\"btn btn-danger btn-sm delete\"  data-id=\""+news.news_id+"\"><i class=\"material-icons\">delete<\/i><\/a><\/td>";
        str += "                    <\/tr>";
        $("#data-body").append(str);
      });
    }
  });
}

$(document).delegate(".delete", "click", function() {
  if(!confirm("Are you sure you want to delete?")){return false;}
  var id = $(this).attr("data-id");
  FUNC.deleteNews(id, access_token, function(err, data) {
    if(!err) {
      showSuccess("News Deleted Successfully!");
      getNews();
    }
  })
})

})// Document Ready
