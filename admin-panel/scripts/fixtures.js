$(function() {

getFixtures();

function getFixtures() {
  FUNC.getAllFixtures(function(err, data) {
    if(!err) {
      $("#data-body").html('');
      data.data.map(function(data) {
        var str="";
        var date = moment(data.date).format('LL');
        var score1 = (data.teamA_score == null)?'N/A':data.teamA_score;
        var score2 = (data.teamB_score == null)?'N/A':data.teamB_score;
        var price = "N/A";
        var count = "N/A";
        if(parseInt(data.tickets_enabled)) {
          price = data.price;
          count = data.tickets_count;
        }
        str += "<tr>";
        str += "                      <td>"+date+"<\/td>";
        str += "                      <td>"+data.teamA+"<\/td>";
        str += "                      <td>"+data.teamB+"<\/td>";
        str += "                      <td>"+score1+"<\/td>";
        str += "                      <td>"+score2+"<\/td>";
        str += "                      <td>"+price+"<\/td>";
        str += "                      <td class=\"text-right\"><a class=\"btn btn-success btn-sm\"  href=\"update-result.php?id="+data.fixture_result_id+"\"><i class=\"material-icons\">remove_red_eye<\/i><\/a> <a class=\"btn btn-danger btn-sm delete\"  data-id=\""+data.fixture_result_id+"\"><i class=\"material-icons\">delete<\/i><\/a><\/td>";
        str += "                    <\/tr>";
        $("#data-body").append(str);
      });
      $("#datatables").DataTable();
    }
  })
}



$(document).delegate(".delete", "click", function() {
  var id = $(this).attr("data-id");
  FUNC.deleteFixture(id, access_token, function(err, data) {
    if(!err) {
      showSuccess("Fixture Deleted Successfully!");
      getFixtures();
    }
  })
})

});
