$(function() {
  $("#tickets_enabled").change(function() {
    var status = $(this).val();
    (status=='true')?$(".ticket-details").show():$(".ticket-details").hide();
  })


  $("#add").click(function(e) {
    e.preventDefault();
    var teamA_id = $("#teamA_id").val() || null;
    var teamB_id = $("#teamB_id").val() || null;
    var date = $("#date").val() || null;
    var location = $("#location").val();
    var tickets_enabled = $("#tickets_enabled").val() || null;
    var price = $("#price").val();
    var tickets_count = $("#tickets_count").val() || 0;
    var time =  $("#time").val();
    var arr = [isNull(teamA_id, "Team A"), isNull(teamB_id, "Team B"), isNull(date, "date"), time.isBlank("Time"), location.isBlank("Location"), isNull(tickets_enabled, "Tickets")];
    if(tickets_enabled == 'true') {
      arr.push(price.isBlank("Price"));
      tickets_enabled = 1;
    } else {
      price = 0;
      tickets_count = 0;
      tickets_enabled = 0;
    }
    if(checkEmpty(arr)){
      return false;
    }
    if(teamA_id == teamB_id) {
      showError("Matches Cannot be played between same team");
      return false;
    }
    if(isNaN(price)){
      showError("Price should be in number");
      return false;
    }
    date = moment(date).format('YYYY/MM/DD');

    FUNC.addFixture({
      teamA_id, teamB_id, date, location, location, tickets_enabled, price, no_of_tickets: tickets_count, access_token, time
    }, function(err, data) {
      if(!err) {
        showSuccess("Fixture Added Successfully!");
        console.log(data);
        setTimeout(function () {
          window.location.href="fixtures.php";
        }, 2000);
      } else {
        showError(err);
      }
    })


  })


  FUNC.getAllTeams(function(err, data) {
    if(!err) {
      var team = prepare_selectpicker(data.data);
      $(".team_list").html(team);
      $('.team_list').selectpicker({
        size: 7,
      });
    }
  }); // get all team


})// Document Ready
