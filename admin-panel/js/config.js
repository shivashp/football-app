var settimeout = 1000;

$(".logout").click(logout);

function logout() {
  localStorage.clear();
  window.location.href="index.html";
}

function toggle_top_menu(className = '.form-input') {
  $(className).slideToggle("slow");
  $('.sp-add-btn').html($('.sp-add-btn').text() == 'Add' ? 'Close' : 'Add');
}

// Slides the top menu down
function slideMenu(className = '.form-input', btnName = '.sp-add-btn'){
  $(className).slideDown();
  $(btnName).html('Close');
  $(btnName).attr("value", 1);
}

//Pulls the top menu up
function pullMenu(className = '.form-input', btnName = '.sp-add-btn', name = '') {
  $(className).slideUp("slow");
  $(btnName).html('Add '+ name);
  $(btnName).attr("value", 0);
  $("#add").attr("status", 0);
}

// Displays the error notification
function showError(message) {
  demo.showNotification('top','right',message, 'danger');
}

//Displays the success notification
function showSuccess(message) {
  demo.showNotification('top','right',message, 'success');
}

// Checks if the string is blank
String.prototype.isBlank = function (type = 'Field') {
  var value = this.toString();
  value = value.trim();
  if (value == '' || (/^\s*$/.test(value))) {
    showError(type + " Cannot Be Empty");
    return false;
  }
  else
  return true;
};

// Checks if the string is equal to another string
String.prototype.equals = function (value2, type = 'Data') {
  var value1 = this.toString();
  if (value1 != value2) {
    showError(type + " Do Not Match");
    return false;
  }
  else
  return true;
};

// Checks if the string is valid email
String.prototype.isEmail = function (type = 'Email') {
  var value = this.toString();
  var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
  if (!(testEmail.test(value))) {
    showError(type + " Should Be Valid");
    return false;
  }
  else
  return true;
};
var isNull = function(value, type = 'Field') {
  if(value === null) {
    showError(" Please Choose " + type);
    return false;
  }
  return true;
}

function parseURL() {
  var str = window.location.search;
  var objURL = {};

  str.replace(
      new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
      function( $0, $1, $2, $3 ){
          objURL[ $1 ] = $3;
      }
  );
  return objURL;
}

// Manage active state of dropdown menus
$(document).delegate(".select-with-transition", "focusin", function() {
  var selector = $(this).attr("data-id");
  $("button[data-id=\""+selector+"\"]").addClass("activeSelect");
})
$(document).delegate(".select-with-transition", "focusout", function() {
  var selector = $(this).attr("data-id");
  $("button[data-id=\""+selector+"\"]").removeClass("activeSelect");
})

// Switch nav link active status
function active(link, className = "link"){
  $(document).ready(function() {
    $("." + className).removeClass("active");
    $("."+link).addClass("active");
    if(className == 'link') {
      $("."+link).find('div').addClass('in');
    }
  });
}

function checkEmpty(arr){
	arr = arr.filter(data => {return !data})
	if(arr.length > 0){
		return true;
	}
	return false;
}

$(document).ready(function() {
  $(".sp-add-btn").click(function() {
    $(".form-horizontal")[0].reset();
    var value = $(this).attr("value");
    (value == 1)?pullMenu():slideMenu();
  });//sp-add-btn
  var nav_username = localStorage.getItem('username') || 'user';
  $("#nav-username").html(nav_username);
})//Document



function prepare_selectpicker(obj) {
  var str = obj.map(obj => {
    return `<option value = "${obj.team_id}">${obj.name}</option>`;
  })
  str = str.join('');
  return str;
}

function _calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}
