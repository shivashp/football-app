<?php include('header.php'); ?>
<div class="content">
<div class="agencyemp-write row">
  <a href="add-fixture.php" class="btn btn-success pull-right sp-add-btn">Add Fixtures</a>
</div><!-- row-->
  <div class="card">
      <div class="card-header card-header-icon" data-background-color="purple">
        <i class="material-icons">event_note</i>
      </div>
      <div class="card-content">
          <h4 class="card-title">Fixture &amp; Results</h4>
          <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                      <tr>
                        <th>Date</th>
                        <th>Team A</th>
                        <th>Team B</th>
                        <th>Team A Score</th>
                        <th>Team B Score</th>
                        <th>Price</th>
                        <th class="disabled-sorting text-right">Actions</th>
                      </tr>
                  </thead>
                  <tbody id="data-body">
                  </tbody>
              </table>
          </div>
      </div> <!-- end content-->
  </div> <!--  end card  -->
</div><!-- content -->
<?php include('footer.php'); ?>
<script src="scripts/fixtures.js" charset="utf-8"></script>
<script>
  active('fixtures');
</script>
<style media="screen">
  #data-body .featured-image {
    max-width:150px;
  }
</style>
