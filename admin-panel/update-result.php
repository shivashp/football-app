<?php  include('header.php'); ?>
<style>
  .btn-group.bootstrap-select {
    margin-top: 2px;
  }
  label.control-label.date-label {
    font-size: 14px;
    margin-top: -27px;
    margin-bottom: 8px;
}
</style>
<script>
  var id = "<?php echo $_GET['id']; ?>";
  if(id == null || id == undefined || id == '') {
    window.location.href="fixtures.php";
  }
</script>
<div class="content">
  <div class="card">
      <div class="card-header card-header-icon" data-background-color="purple">
          <i class="material-icons">person</i>
      </div>
      <div class="card-content">
          <h4 class="card-title">Update Result</h4>
          <form class="form-horizontal" id="update-fixture" name="update-fixture">

            <div class="row add-emp-row">

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label" id="teamA_label"></label>
                    <input type="number" name="" class="form-control" id="teamA_score" value="">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

              <div class="col-md-4">
                <div class="form-group label-floating">
                <label class="control-label" id="teamB_label"></label>
                    <input type="number" name="" class="form-control" id="teamB_score" value="">
                    <div class="help-block with-errors"></div>
                </div>
              </div><!-- col-md-4-->

            </div><!--row -->

            <div class="row text-center" style="margin-top:20px;">
              <button type="submit" name="Save" class="btn btn-success" id="add">Save</button>
              <input type="reset" class="btn btn-danger" name="Clear">
            </div>
          </form>
      </div> <!-- end content-->
  </div> <!--  end card  -->
</div><!-- content -->
<?php include('footer.php'); ?>
<script>
  $().ready(function() {
      demo.initFormExtendedDatetimepickers();
      active("fixtures")
  });
</script>
<script src="scripts/update-result.js" charset="utf-8"></script>
