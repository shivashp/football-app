<?php include('header.php'); ?>
<div class="content">
<div class="agencyemp-write row">
  <a href="add-news.php" class="btn btn-success pull-right sp-add-btn">Add News</a>
</div><!-- row-->
  <div class="card">
      <div class="card-header card-header-icon" data-background-color="purple">
        <i class="material-icons">library_books</i>
      </div>
      <div class="card-content">
          <h4 class="card-title">News</h4>
          <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                      <tr>
                        <th>Featured</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th class="disabled-sorting text-right">Actions</th>
                      </tr>
                  </thead>
                  <tbody id="data-body">
                  </tbody>
              </table>
          </div>
      </div> <!-- end content-->
  </div> <!--  end card  -->
</div><!-- content -->
<?php include('footer.php'); ?>
<script src="scripts/news.js" charset="utf-8"></script>
<script>
  active('news');
</script>
<style media="screen">
  #data-body .featured-image {
    max-width:150px;
  }
</style>
