<?php include('header.php'); ?>

<script>
  var slug = "<?php echo $_GET['news']; ?>";
</script>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>NEWS &amp; EVENTS</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">News &amp; Events</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content" id="news-events">
  <div class="container">
      <div class="news">
        <div class="image">
          <img src="" id="featured">
        </div>
        <div class="title">
          <a href="javascript:;" id="title"></a>
        </div>
        <div class="meta">
          by Admin | <span id='date'></span>
        </div>
        <div class="body" id="body">
        </div>
      </div><!-- news -->
    </div><!-- row -->
  </div><!-- container -->
</section><!-- news-events -->

<?php include('footer.php'); ?>
<script src="scripts/single-news.js" charset="utf-8"></script>
