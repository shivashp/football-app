<?php include("header.php"); ?>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>FIXTURES</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Fixtures</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content fixture-result" id="fixture">
  <div class="container" id="data-body">
  </div><!-- container -->
</section><!-- fixture-result -->

<?php include("footer.php"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js" charset="utf-8"></script>
<script src="scripts/fixture.js" charset="utf-8"></script>
