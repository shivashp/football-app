$(function() {

  $("#save").click(function(e) {
    e.preventDefault();
    var photo = $("#photo").val() || null;
    var name = $("#name").val();
    var age =  $("#age").val();
    var address = $("#address").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var policy = $("#policy").is(':checked');
    var arr = [isNull(photo, "Photo"), name.isBlank("Name"), age.isBlank("Age")];
    var arr1 =[address.isBlank("Address"), email.isBlank("Email"), email.isEmail("Email"), password.isBlank("Password")];
    if(checkEmpty(arr)){
      return false;
    }
    if(checkEmpty(arr1)){
      return false;
    }
    if(!policy) {
      showError("Please agree to terms and conditions");
      return false;
    }
    var formData = new FormData(document.forms.namedItem("user-register"));
    $("#save").attr("value", "CREATING ACCOUNT...");
    $("#save").attr("disabled", true);
    FUNC.registerUser(formData, function(err, data) {
      if(!err) {
        showSuccess("Registration Successful!");
        setTimeout(function () {
          window.location.href="login.php";
        }, 2000);
      }
      $("#save").attr("value", "CREATING NEW ACCOUNT");
      $("#save").attr("disabled", false);      
    });

  })

});// Save
