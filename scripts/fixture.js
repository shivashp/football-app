$(function() {

FUNC.getAllFixtures(function(err, data) {
  if(!err) {
    if(data.data.length == 0) {
      NoFxiture();
      return false;
    }
    var data = data.data.filter(function(data){
      var valid = true;
      var today = new Date();
      var time = data.time.split(' ');
      let timeObj = time[0];
      if(time[1] == 'PM') {
        timeObj = timeObj.split(':');
        timeObj[0] = (parseInt(timeObj[0]) + 12).toString();
        timeObj = timeObj.join(':');
      }
      if(parseInt(data.result) || today > new Date(data.date+' '+timeObj)) {
        valid = false;
      }
      return valid;
    });
    if(data.length == 0) {
      NoFxiture();
      return false;
    }
    var groupedResults = _.groupBy(data, function (result) {
      var date = new Date(result['date'])
      return moment(date, 'DD/MM/YYYY').format('LL');
    })

    var keys = Object.keys(groupedResults);
    $("#data-body").html('');
    keys.map(function(data) {
      var str="";
      str += "<div class=\"date\">";
      str += data;
      str += "    <\/div>";
      str += "<div class=\"matches\">";
      groupedResults[data].map(function(match) {
        var time = match.time.split(' ');
        let timeObj = time[0];
        if(time[1] == 'PM') {
          timeObj = timeObj.split(':');
          timeObj[0] = (parseInt(timeObj[0]) + 12).toString();
          timeObj = timeObj.join(':');
        }
        str += "<div class=\"single-match\">";
        str += "        <div class=\"col-md-7 col-sm-12 team-details\">";
        str += "          <div class=\"col-md-3 col-md-offset-2 col-sm-4 col-xs-4\">";
        str += "            <span class=\"team\">"+match.teamA+"<\/span>";
        str += "          <\/div>";
        str += "          <div class=\"col-md-2 col-sm-4 col-xs-4\">";
        str += "            <span class=\"board\">"+timeObj+"<\/span>";
        str += "          <\/div>";
        str += "          <div class=\"col-md-5 col-sm-4 col-xs-4\">";
        str += "            <span class=\"team\">"+match.teamB+"<\/span>";
        str += "          <\/div>";
        str += "        <\/div>";
        str += "        <div class=\"col-md-5 col-sm-12\">";
        str += "          <div class=\"stadium\">";
        str += match.location;
        str += "          <\/div>";
        str += "        <\/div>";
        str += "      <\/div>";

      })
      str += "<\/div>";
      $("#data-body").append(str);
    })
  }
})

function NoFxiture() {
    var str="";
    str += "  <div class=\"no-data\">";
    str += "          No upcomming matches";
    str += "        <\/div>";
    $("#data-body").html(str);
}

});
