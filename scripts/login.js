$(function() {

  $("#login-user").click(function(e) {
    e.preventDefault();
    var email = $("#email").val();
    var password = $("#password").val();
    var arr =[email.isBlank("Email"), email.isEmail("Email"), password.isBlank("Password")];
    if(checkEmpty(arr)){
      return false;
    }
    FUNC.loginUser({
      email, password
    }, function(err, data) {
      if(!err) {
        showSuccess("Login Successful!");
        localStorage.setItem('access_token', data.data.access_token);
        localStorage.setItem('name', data.data.name);
        localStorage.setItem('user_id', data.data.user_id);
        setTimeout(function () {
          window.location.href="tickets.php";
        }, 500);
      }
    })
  })// Login

})// Login
