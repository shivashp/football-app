$(function() {


//========================================= Get Fixture and create Tickets ==============================
FUNC.getAllFixtures(function(err, data) {
  if(!err) {
    if(data.data.length == 0) {
      NoTickets();
      return false;
    }
    var data = data.data.filter(function(data){
      var valid = true;
      var today = new Date();
      var time = data.time.split(' ');
      let timeObj = time[0];
      if(time[1] == 'PM') {
        timeObj = timeObj.split(':');
        timeObj[0] = (parseInt(timeObj[0]) + 12).toString();
        timeObj = timeObj.join(':');
      }
      if(!parseInt(data.tickets_enabled) || parseInt(data.result) || today > new Date(data.date+' '+timeObj)) {
        valid = false;
      }
      return valid;
    });
    if(data.length == 0) {
      NoTickets();
      return false;
    }
    var ticket_obj = data.map(function(data) {
      return {
        id: data.fixture_result_id,
        date: data.date,
        time: data.time,
        team1: data.teamA,
        team2: data.teamB,
        logo1: data.teamA_logo,
        logo2: data.teamB_logo,
        venue: data.location,
        price: data.price,
        count: data.tickets_count
      }
    });
    generateTicket(ticket_obj);
  }
})

function NoTickets() {
    var str="";
    str += "  <div class=\"no-data\">";
    str += "          No tickets available";
    str += "        <\/div>";
    $("#tickets").html(str);
}


const generateTicket = (tickets) => {
  $("#tickets").html(
    tickets.map((data) => {
      var str="";
      str += "<div class=\"ticket\">";
      str += "        <div class=\"col-md-3 pull-right ticket-details\">";
      str += "          <div class=\"timings\">";
      str += "            <div class=\"ticketlabel\">Price:<\/div> $"+data.price;
      str += "          <\/div>";
      str += "          <div class=\"timings\">";
      str += "            <div class=\"ticketlabel\">Date/Time:<\/div> "+data.date + " , "+data.time;
      str += "          <\/div>";
      str += "          <div class=\"venue\">";
      str += "            <div class=\"ticketlabel\">Location: <\/div> "+data.venue;
      str += "          <\/div>";
      str += "        <\/div>";
      str += "        <div class=\"col-md-9\">";
      str += "          <div class=\"title\">";
      str += "            <img src=\""+data.logo1+"\"> "+data.team1+" <span class=\"vs\">Vs<\/span> "+data.team2+" <img src=\""+data.logo2+"\">";
      str += "            <div class=\"buy-button\">";
      str += "              <button type=\"button\"  class=\"btn btn-primary buyTicket\" data-id=\""+data.id+"\" name=\"button\">Buy Ticket<\/button>";
      str += "            <\/div>";
      str += "          <\/div>";
      str += "        <\/div>";
      str += "      <\/div>";
      return str;
    }).join(''));
}

});

$(document).delegate(".buyTicket", "click", function() {
  var id = $(this).attr("data-id");
  if(name !== null && user_access_token !== null) {
    window.location.href="buy-ticket.php?id="+id;
  } else {
    window.location.href="login.php";
  }

})
