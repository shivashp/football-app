$(function() {


$("#send-message").click(function(e) {
  e.preventDefault();
  var name = $("#name").val();
  var email = $("#email").val();
  var message =$("#message").val();
  var arr =[name.isBlank("Name"), email.isBlank("Email"), email.isEmail("Email"), message.isBlank("Message")];
  if(checkEmpty(arr)){
    return false;
  }
  $("#send-message").html("SENDING MESSAGE...");
  $("#send-message").attr("disabled", true);
  FUNC.contact({
    name, email, message
  }, function(err, data){
    console.log(data);
    if(!err) {
      showSuccess("Message Sent Successfully");
      window.location.reload();
    }
    $("#send-message").html("SEND MESSAGE");
    $("#send-message").attr("disabled", false);
  })
})// send-message

})// Document
