$(function() {
getNews();
getFixtures();




function generateTicket(data) {
  data.map(function(data, id) {
    var date = moment(new Date(data.date), 'DD/MM/YYYY').format('L');
    var id = data.fixture_result_id;
    var teamA = data.teamA;
    var teamB = data.teamB;
    var logo1 = data.teamA_logo;
    var logo2 = data.teamB_logo;
    var time = data.time;
    var str="";
    str += "<div class=\"sb-single-ticket\">";
    str += "                    <div class=\"buy-ticket\" data-id='"+id+"'>";
    str += "                      Buy this Ticket";
    str += "                    <\/div>";
    str += "                    <div class=\"date\">";
    str += "                      "+date+" \/ "+time;
    str += "                    <\/div>";
    str += "                    <div class=\"match\">";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo1+"\">";
    str += "                        <div> "+teamA+"<\/div>";
    str += "                      <\/div>";
    str += "                      <div class=\"vs\">vs<\/div>";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo2+"\">";
    str += "                        <div>"+teamB+"<\/div>";
    str += "                      <\/div>";
    str += "                    <\/div>";
    str += "                  <\/div><!-- single-ticket -->";
    $("#sb-ticket-body").append(str);
  })// Data
  $("#sb-ticket-slider").diyslider({
      width: "360", // width of the slider
      height: "335", // height of the slider
      display: 1, // number of slides you want it to display at once
      loop: true, // disable looping on slides
      animationDuration: 400
  }); // this is all you need!
  $("#ticket-go-left").bind("click", function(){
      $("#sb-ticket-slider").diyslider("move", "back");
  });
  $("#ticket-go-right").bind("click", function(){
      $("#sb-ticket-slider").diyslider("move", "forth");
  });
}

$(document).delegate(".buy-ticket", "click", function() {
  var id = $(this).attr("data-id");
  if(name !== null && user_access_token !== null) {
    window.location.href="buy-ticket.php?id="+id;
  } else {
    window.location.href="login.php";
  }
})

function renderSidebarFixture(data) {
  $("#sb-fixture-data").html('');
  data.map(function(data, id) {
    var date = moment(new Date(data.date), 'DD/MM/YYYY').format('L');
    var teamA = data.teamA;
    var teamB = data.teamB;
    var logo1 = data.teamA_logo;
    var logo2 = data.teamB_logo;
    var time = data.time;
    var str="";
    str += "<div class=\"slider-single-fixture\">";
    str += "                    <div class=\"time\" id=\"next-match-time-"+id+"\">";
    str += "                    <\/div>";
    str += "                    <div class=\"date\">";
    str += "                      "+date+" \/ "+time;
    str += "                    <\/div>";
    str += "                    <div class=\"match\">";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo1+"\">";
    str += "                        <div> "+teamA+"<\/div>";
    str += "                      <\/div>";
    str += "                      <div class=\"vs\">vs<\/div>";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo2+"\">";
    str += "                        <div>"+teamB+"<\/div>";
    str += "                      <\/div>";
    str += "                    <\/div>";
    str += "                  <\/div>";
    $("#sb-fixture-data").append(str);
    new startCounter(data.date, "next-match-time-"+id);
  });
  $("#single-fixture-slider").diyslider({
      width: "360", // width of the slider
      height: "335", // height of the slider
      display: 1, // number of slides you want it to display at once
      loop: true, // disable looping on slides
      animationDuration: 400
  }); // this is all you need!
  $("#fixture-go-left").bind("click", function(){
      $("#single-fixture-slider").diyslider("move", "back");
  });
  $("#fixture-go-right").bind("click", function(){
      $("#single-fixture-slider").diyslider("move", "forth");
  });
}


$(".subscribe-btn").click(function(e) {
  e.preventDefault();
  var email = $("#subs-email").val();
  if(!email.isBlank("Email") || !email.isEmail("Email")) {
    return false;
  }
  $("#subscribe-btn").html("Loading...");
  FUNC.subscribe(email, function(err, data) {
    if(!err) {
      showSuccess("Subscribed Successfully!");
      $("#subs-email").val('');
    }
    $("#subscribe-btn").html("SUBSCRIBE");
  });
});


function getFixtures() {
  FUNC.getAllFixtures(function(err, fixtures) {
    if(!err) {
      if(fixtures.data.length == 0) {
        NoDataSidebar("No upcomming matches", "#sb-fixture-data");
        NoDataSidebar("No tickets available", "#sb-ticket-body");
        return false;
      }
      var data = fixtures.data.filter(function(data){
        var valid = true;
        var today = new Date();
        var time = data.time.split(' ');
        let timeObj = time[0];
        if(time[1] == 'PM') {
          timeObj = timeObj.split(':');
          timeObj[0] = (parseInt(timeObj[0]) + 12).toString();
          timeObj = timeObj.join(':');
        }
        if(parseInt(data.result) || today > new Date(data.date+' '+timeObj)) {
          valid = false;
        }
        return valid;
      });

      if(data.length == 0) {
        NoDataSidebar("No upcomming matches", "#sb-fixture-data");
        NoDataSidebar("No tickets available", "#sb-ticket-body");
        return false;
      }

      renderSidebarFixture(data);

      var str="";
      str += "<div class=\"list odd head\">";
      str += "                <div class=\"col-md-3 col-sm-6 col-xs-6\">";
      str += "                  DATE";
      str += "                <\/div>";
      str += "                <div class=\"col-md-9 col-sm-6 col-xs-6\">";
      str += "                  TEAM";
      str += "                <\/div>";
      str += "              <\/div><!-- row -->";
      $("#fixture-body").html(str);
      data.map(function(data,i) {
        var date = moment(new Date(data.date), 'DD/MM/YYYY').format('ll');
        var teamA = data.teamA;
        var teamB = data.teamB;
        var logo1 = data.teamA_logo;
        var logo2 = data.teamB_logo;
        var str="";
        var className = (i%2==0)?'list even':'list odd';
        str += "<div class=\""+className+"\">";
        str += "                  <div class=\"col-md-3 col-sm-6 col-xs-6 left\">";
        str += date;
        str += "                  <\/div>";
        str += "                  <div class=\"col-md-9 col-sm-6 col-xs-6 right\">";
        str += "                    <span class=\"team\">";
        str += "                      <img src=\""+logo1+"\">"+teamA;
        str += "                    <\/span>";
        str += "                    <span class=\"vs\">Vs<\/span>";
        str += "                    <span class=\"team\">";
        str +=teamB + "<img src=\""+logo2+"\" style='margin-left:15px'>";
        str += "                    <\/span>";
        str += "                  <\/div>";
        str += "                <\/div><!-- row -->";
        $("#fixture-body").append(str);
      });

      var tickets = data.filter(function(data){
        return parseInt(data.tickets_enabled);
      });
      if(tickets.length == 0) {
        NoTickets();
        return false;
      }
      generateTicket(tickets);
    }
  });
}

function NoFxiture() {
    var str="";
    str += "  <div class=\"no-data\">";
    str += "          No upcomming matches";
    str += "        <\/div>";
    $("#fixture-body").html(str);
}

function NoDataSidebar(message,elem) {
  var str="";
  str += "  <div class=\"no-data\">";
  str += message;
  str += "        <\/div>";
  $(elem).html(str);
}

function getNews() {
  FUNC.getAllNews(function(err, data) {
    if(!err) {
      if(data.data.length == 0) {
        noNews();
        return false;
      }
      var data = data.data;
      var str="";
      var length = (data.length > 3)?3:data.length;
      for (var i = 0; i < length; i++) {
        var str = '';
        var newsClass= (i==0)?'news news-landscape':'news';
        var featured = data[i].featured_image;
        var news_id = data[i].news_id;
        var title = data[i].title;
        var slug = data[i].slug;
        var desc = (data[i].description.length > 100)?data[i].description.substr(0,100)+'...':data[i].description;
        desc = desc.replace(/<(?:.|\n)*?>/gm, '');
        var created_at = moment(data[i].created_at).fromNow();
        str += "<div class=\""+newsClass+"\">";
        str += "                <div class=\"image\">";
        str += "                  <img src=\""+featured+"\">";
        str += "                <\/div><!--image -->";
        str += "                <div class=\"details\">";
        str += "                  <div class=\"author\"> <img src=\"img\/icons\/admin.png\"> <b>Posted by <\/b>- Admin<\/div>";
        str += "                  <div class=\"header\" data-slug='"+slug+"'>"+title+"<\/div>";
        str += "                  <div class=\"desc\">"+desc+"<\/div>";
        str += "                  <div class=\"footer\">";
        str += "                    <div class=\"time\">";
        str += "                    <img src=\"img\/icons\/clock.png\" alt=\"\"> &nbsp; <span>"+created_at+"<\/span>";
        str += "                    <\/div>";
        str += "                    <div class=\"button\">";
        str += "                      <a href=\"#\"><i class=\"fa fa-chevron-right\"><\/i><\/a>";
        str += "                    <\/div>";
        str += "                  <\/div>";
        str += "                <\/div><!-- details -->";
        str += "              <\/div><!-- news-landscape -->";
        if(i !== 0) {
          str = wrapInRow(str);
          $("#news-body1").append(str);
        } else {
          $("#news-body").append(str);
          str ='';
        }
      }
    }
  })
}

$(document).delegate(".header", "click", function() {
  var slug = $(this).attr("data-slug");
  if(slug == '' || slug == null || slug == undefined) {
    return false;
  }
  window.location.href = 'single-news.php?news='+slug;
})


function wrapInRow(data) {

  var str="";
  str += "<div class=\"col-md-6 col-sm-12 col-xs-12\">";
  str += data;
  str += "<\/div>";
  return str;
}

function noNews() {
  var str="";
  str += "  <div class=\"no-data\">";
  str += "          No News available";
  str += "        <\/div>";
  $("#news-body").html(str);
}

})// Document
