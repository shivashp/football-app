(function() {


FUNC.getSingleFixture(id, function(err, data) {
  if(!err) {    
    var data = data.data;
    var date = data.date + " , "+data.time;
    var location = data.location;
    var price = "$" + data.price;
    var str = "            <img src=\""+data.teamA_logo+"\"> "+data.teamA+" <span class=\"vs\">Vs<\/span> "+data.teamB+" <img src=\""+data.teamB_logo+"\">";
    $("#t-price").html(price);
    $("#t-time").html(date);
    $("#t-location").html(location);
    $("#team-title").html(str);
  }
})


  $('#payment-form').card({
    container: '.card-wrapper', // *required*
  });


//================================= Stripe ==================================== //
  Stripe.setPublishableKey('pk_test_kUKu2CpsMYlUAJG6GwQW9Z9y');

  $('#payment-form').submit(function(e) {
    e.preventDefault();
    generateToken();
  });

  var generateToken = function() {
    $(".payment-errors").html('');
      var form = $("#payment-form");
      // No pressing the buy now button more than once
      form.find('button').prop('disabled', true);
      $("#pay").attr("disabled", true);
      $("#pay").html("Loading...");

      // Create the token, based on the form object
      Stripe.createToken(form, stripeResponseHandler);

  };

  var stripeResponseHandler = function(status, response) {
    var form = $('#payment-form');
      if (response.error) {
          $(".payment-errors").html(response.error.message);
          $("#pay").attr("disabled", false);
          $("#pay").html("Pay");
      } else {
          FUNC.chargeUser({
            charge_id: response.id,
            user_id: localStorage.getItem("user_id"),
            ticket_id: id
          }, function(err, data) {
            if(!err) {
              if(data.status) {
                $('.payment-success').html(data.message);
                setTimeout(function () {
                  window.location.href="user-profile.php";
                }, 2000);
              } else {
                form.find('.payment-errors').text(data.message);
              }
            } else {
              form.find('.payment-errors').text(err.message);
            }
          });
      }
  };

})();
