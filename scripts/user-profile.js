$(function() {
var USER_ID = localStorage.getItem('user_id');
var TICKETS;
getUserDetails();
getTickets();



function getUserDetails() {

  FUNC.getSingleUser(USER_ID, function(err, data) {
    if(!err) {
      var data = data.data;
      var name = data.name;
      var image = data.photo;
      var address = data.address;
      $("#name").html(name);
      $("#profile-address").html(address);
      $("#user-image>img").attr("src", image);
      $("#print-user-name").html(name);
    }
  })
}

function getTickets() {
  FUNC.getUserTickets(USER_ID, function(err, data) {
    if(!err) {
      if(data.data.length == 0) {
        NoTickets();
        return false;
      }
      TICKETS = data.data;
      data.data.map(function(data,i) {
        var teamA = data.teamA;
        var teamB = data.teamB;
        var date = moment(new Date(data.date), 'DD/MM/YYYY').format('LL');
        var time = data.time;
        var location =data.location;
        var price = data.price;
        var status = '<i class=\"label label-success\">upcomming<\/i>';
        var today = new Date();
        today = today.setHours(0,0,0,0)
        if(parseInt(data.result) || today > new Date(data.date)) {
          status = '<i class=\"label label-danger\">expired<\/i>'
        }
        var str="";
        str += "<tr>";
        str += "            <td>"+date+"<\/td>";
        str += "            <td>"+time+"<\/td>";
        str += "            <td>"+teamA+" vs "+teamB+"<\/td>";
        str += "            <td>"+location+"<\/td>";
        str += "            <td>$"+price+"<\/td>";
        str += "            <td>"+status+"<\/td>";
        str += "            <td><button data-index = \""+i+"\" type=\"button\" name=\"button\" class=\"btn-primary btn-sm print\"><i class=\"fa fa-print\"><\/i><\/button><\/td>";
        str += "          <\/tr>";
        $("#table-body").append(str);
      })
    }
  })
}// tickets


$(document).delegate(".print", "click", function() {
  var i = $(this).attr("data-index");
  var ticket = TICKETS[i];
  var teams = ticket.teamA + " <span>vs</span> " + ticket.teamB;
  var date = moment(new Date(ticket.date), 'DD/MM/YYYY').format('LL');
  var location = ticket.location;
  var time = ticket.time;
  var price = ticket.price;
  $("#print-team-names").html(teams);
  $("#print-team-date").html(date);
  $("#print-team-location").html(location);
  $("#print-team-time").html(time);
  $("#print-price").html('$'+price);  

  var printContents = document.getElementById('printableArea').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
});


function NoTickets() {
  var str="";
  str += "  <td colspan='7' class=\"no-data\">";
  str += "          No Tickets Purchased Yet!";
  str += "        <\/td>";
  $("#table-body").html(str);
}

})// Document
