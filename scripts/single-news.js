$(function() {

getNews();
  function getNews() {
    FUNC.getSingleNews(slug, function(err, data) {
      if(!err) {
        $("#news-body").html('');
        console.log(data);
        var news = data.data.data[0];
        var title =news.title;
        var desc = news.description;
        var created_at = moment(news.created_at).fromNow();
        var featured_image = news.featured_image;

        $("#title").html(title);
        $("#featured").attr("src", featured_image);
        $("#date").html(created_at);
        $("#body").html(desc);
      }
    });
  }



})// Document
