$(function() {

  console.log("Player " , player_id);

  FUNC.getSinglePlayer(player_id, function(err, data) {
    if(!err) {
      var player = data.data[0];
      var name = player.first_name + " " + player.last_name;
      var squad_no = player.squad_no;
      var image = player.image;
      var featured = player.featured_image;
      var position = player.position;
      var dob = player.dob;
      var age = player.age;
      var description = player.description;
      var team = player.name;

      $("#title-player-name").html(name);
      $("#title-squad-no").html(squad_no);
      $("#name").html(name);
      $("#squad_no").html(squad_no);
      $("#team").html(team);
      $("#position").html(position);
      $("#dob").html(dob);
      $("#age").html(age);
      $("#desc").html(description);
      $("#player-image").attr("src", image);
      $("#player-slider").css("background-image",'url(' + featured + ')' )

    }
  })

})// Document
