$(function() {

getNews();
getFixtures();


function generateTicket(data) {
  data.map(function(data, id) {
    var date = moment(new Date(data.date), 'DD/MM/YYYY').format('L');
    var id = data.fixture_result_id;
    var teamA = data.teamA;
    var teamB = data.teamB;
    var logo1 = data.teamA_logo;
    var logo2 = data.teamB_logo;
    var time = data.time;
    var str="";
    str += "<div class=\"sb-single-ticket\">";
    str += "                    <div class=\"buy-ticket\" data-id='"+id+"'>";
    str += "                      Buy this Ticket";
    str += "                    <\/div>";
    str += "                    <div class=\"date\">";
    str += "                      "+date+" \/ "+time;
    str += "                    <\/div>";
    str += "                    <div class=\"match\">";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo1+"\">";
    str += "                        <div> "+teamA+"<\/div>";
    str += "                      <\/div>";
    str += "                      <div class=\"vs\">vs<\/div>";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo2+"\">";
    str += "                        <div>"+teamB+"<\/div>";
    str += "                      <\/div>";
    str += "                    <\/div>";
    str += "                  <\/div><!-- single-ticket -->";
    $("#sb-ticket-body").append(str);
  })// Data
  $("#sb-ticket-slider").diyslider({
      width: "360", // width of the slider
      height: "335", // height of the slider
      display: 1, // number of slides you want it to display at once
      loop: true, // disable looping on slides
      animationDuration: 400
  }); // this is all you need!
  $("#ticket-go-left").bind("click", function(){
      $("#sb-ticket-slider").diyslider("move", "back");
  });
  $("#ticket-go-right").bind("click", function(){
      $("#sb-ticket-slider").diyslider("move", "forth");
  });
}

$(document).delegate(".buy-ticket", "click", function() {
  var id = $(this).attr("data-id");
  if(name !== null && user_access_token !== null) {
    window.location.href="buy-ticket.php?id="+id;
  } else {
    window.location.href="login.php";
  }
})

function renderSidebarFixture(data) {
  $("#sb-fixture-data").html('');
  data.map(function(data, id) {
    var date = moment(new Date(data.date), 'DD/MM/YYYY').format('L');
    var teamA = data.teamA;
    var teamB = data.teamB;
    var logo1 = data.teamA_logo;
    var logo2 = data.teamB_logo;
    var time = data.time;
    var str="";
    str += "<div class=\"slider-single-fixture\">";
    str += "                    <div class=\"time\" id=\"next-match-time-"+id+"\">";
    str += "                    <\/div>";
    str += "                    <div class=\"date\">";
    str += "                      "+date+" \/ "+time;
    str += "                    <\/div>";
    str += "                    <div class=\"match\">";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo1+"\">";
    str += "                        <div> "+teamA+"<\/div>";
    str += "                      <\/div>";
    str += "                      <div class=\"vs\">vs<\/div>";
    str += "                      <div class=\"team\">";
    str += "                        <img src=\""+logo2+"\">";
    str += "                        <div>"+teamB+"<\/div>";
    str += "                      <\/div>";
    str += "                    <\/div>";
    str += "                  <\/div>";
    $("#sb-fixture-data").append(str);
    new startCounter(data.date, "next-match-time-"+id);
  });
  $("#single-fixture-slider").diyslider({
      width: "360", // width of the slider
      height: "335", // height of the slider
      display: 1, // number of slides you want it to display at once
      loop: true, // disable looping on slides
      animationDuration: 400
  }); // this is all you need!
  $("#fixture-go-left").bind("click", function(){
      $("#single-fixture-slider").diyslider("move", "back");
  });
  $("#fixture-go-right").bind("click", function(){
      $("#single-fixture-slider").diyslider("move", "forth");
  });
}

function getFixtures() {
  FUNC.getAllFixtures(function(err, fixtures) {
    if(!err) {
      if(fixtures.data.length == 0) {
        NoDataSidebar("No upcomming matches", "#sb-fixture-data");
        NoDataSidebar("No tickets available", "#sb-ticket-body");
        return false;
      }
      var data = fixtures.data.filter(function(data){
        var valid = true;
        var today = new Date();
        var time = data.time.split(' ');
        let timeObj = time[0];
        if(time[1] == 'PM') {
          timeObj = timeObj.split(':');
          timeObj[0] = (parseInt(timeObj[0]) + 12).toString();
          timeObj = timeObj.join(':');
        }
        if(parseInt(data.result) || today > new Date(data.date+' '+timeObj)) {
          valid = false;
        }
        return valid;
      });

      if(data.length == 0) {
        NoDataSidebar("No upcomming matches", "#sb-fixture-data");
        NoDataSidebar("No tickets available", "#sb-ticket-body");
        return false;
      }
      renderSidebarFixture(data);
      var tickets = data.filter(function(data){
        return parseInt(data.tickets_enabled);
      });
      if(tickets.length == 0) {
        NoTickets();
        return false;
      }
      generateTicket(tickets);
    }
  });
}

function NoFxiture() {
    var str="";
    str += "  <div class=\"no-data\">";
    str += "          No upcomming matches";
    str += "        <\/div>";
    $("#fixture-body").html(str);
}

function NoDataSidebar(message,elem) {
  var str="";
  str += "  <div class=\"no-data\">";
  str += message;
  str += "        <\/div>";
  $(elem).html(str);
}

  function getNews() {
    FUNC.getAllNews(function(err, data) {
      if(!err) {
        $("#news-body").html('');
        if(data.data.length == 0) {
          noNews();
          return false;
        }
        data.data.map(function(news) {
          var str="";
          var desc = (news.description.length > 300)?news.description.substr(0,300)+'...':news.description;
          desc = desc.replace(/<(?:.|\n)*?>/gm, '');
          var created_at = moment(news.created_at).fromNow();
          str += "<div class=\"news\">";
          str += "          <div class=\"image\">";
          str += "            <img src=\""+news.featured_image+"\">";
          str += "          <\/div>";
          str += "          <div class=\"title\">";
          str += "            <a href=\"single-news.php?news="+news.slug+"\">"+news.title+"<\/a>";
          str += "          <\/div>";
          str += "          <div class=\"meta\">";
          str += "            by Admin | "+created_at;
          str += "          <\/div>";
          str += "          <div class=\"body\">";
          str += "            <p>"+desc+"<\/p>";
          str += "          <\/div>";
          str += "        <\/div>";

          $("#news-body").append(str);
        });
        $('#news-body').paginate({itemsPerPage: 3});
      }
    });
  }

  function noNews() {
      var str="";
      str += "  <div class=\"no-data\">";
      str += "          No news found. Please check later";
      str += "        <\/div>";
      $("#news-body").html(str);
  }

})// Document
