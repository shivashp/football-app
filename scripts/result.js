$(function() {

FUNC.getAllFixtures(function(err, data) {
  if(!err) {
    if(data.data.length == 0) {
      NoFxiture();
      return false;
    }
    var data = data.data.filter(function(data){
      return parseInt(data.result);
    });
    if(data.length == 0) {
      NoFxiture();
      return false;
    }
    var groupedResults = _.groupBy(data, function (result) {
      var date = new Date(result['date'])
      return moment(date, 'DD/MM/YYYY').format('LL');
    })

    var keys = Object.keys(groupedResults);
    $("#data-body").html('');
    keys.map(function(data) {
      var str="";
      str += "<div class=\"date\">";
      str += data;
      str += "    <\/div>";
      str += "<div class=\"matches\">";
      groupedResults[data].map(function(match) {
        str += "<div class=\"single-match\">";
        str += "        <div class=\"col-md-8 col-sm-12 team-details\">";
        str += "          <div class=\"col-md-3 col-md-offset-1 col-sm-4 col-xs-4\">";
        str += "            <span class=\"team\">"+match.teamA+"<\/span>";
        str += "          <\/div>";
        str += "          <div class=\"col-md-3 col-sm-4 col-xs-4\">";
        str += "            <span class=\"board\">"+match.teamA_score+"-"+match.teamB_score+"<\/span>";
        str += "          <\/div>";
        str += "          <div class=\"col-md-5 col-sm-4 col-xs-4\">";
        str += "            <span class=\"team\">"+match.teamB+"<\/span>";
        str += "          <\/div>";
        str += "        <\/div>";
        str += "        <div class=\"col-md-4 col-sm-12\">";
        str += "          <div class=\"stadium\">";
        str += match.location;
        str += "          <\/div>";
        str += "        <\/div>";
        str += "      <\/div>";

      })
      str += "<\/div>";
      $("#data-body").append(str);
    })
  }
})

function NoFxiture() {
    var str="";
    str += "  <div class=\"no-data\">";
    str += "          No results to display";
    str += "        <\/div>";
    $("#data-body").html(str);
}

});
