$(function() {

$(".team-name").html(team_name);
getTeamDetails();
  function getTeamDetails() {
    FUNC.getPlayersByTeam(team_id, function(err, data) {
      console.log(data);
      var goalkeepers = [];
      var midfielders = [];
      var defenders = [];
      var forwards = [];
      if(!err && data.data.length > 0) {
        goalkeepers = data.data.filter((player) => player.position == 'goalkeeper');
        midfielders = data.data.filter((player) => player.position == 'midfielder');
        defenders = data.data.filter((player) => player.position == 'defender');
        forwards = data.data.filter((player) => player.position == 'forward');
        $("#goalkeepers").html(goalkeepers.map((player) => playerCard(player)).join(''));
        $("#midfielders").html(midfielders.map((player) => playerCard(player)).join(''));
        $("#defenders").html(defenders.map((player) => playerCard(player)).join(''));
        $("#forwards").html(forwards.map((player) => playerCard(player)).join(''));
      }
      NoPlayer(goalkeepers, "goalkeepers");
      NoPlayer(midfielders, "midfielders");
      NoPlayer(defenders, "defenders");
      NoPlayer(forwards, "forwards");
    });
  }


function playerCard(player) {
  var str="";
  str += "<div class=\"col-md-3 col-sm-6 col-xs-12\">";
  str += "          <a href=\"single-player.php?id="+player.player_id+"\">";
  str += "            <div class=\"player-card\">";
  str += "              <div class=\"image\">";
  str += "                <img src=\""+player.image+"\">";
  str += "              <\/div>";
  str += "              <div class=\"details\">";
  str += "                <div class=\"col-xs-8\">";
  str += "                  <div class=\"first-name\">"+player.first_name+"<\/div>";
  str += "                  <div class=\"last-name\">"+player.last_name+"<\/div>";
  str += "                <\/div>";
  str += "                <div class=\"col-xs-4\">";
  str += "                  <div class=\"squad-no\">"+player.squad_no+"<\/div>";
  str += "                <\/div>";
  str += "              <\/div>";
  str += "            <\/div>";
  str += "          <\/a>";
  str += "        <\/div>";
  return str;
}

function NoPlayer(arr, name) {  
  if(arr.length == 0) {
    var str="";
    str += "  <div class=\"no-data\">";
    str += "          No players found!";
    str += "        <\/div>";
    $("#"+name).html(str);
  }
}

})// Document
