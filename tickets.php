<?php include('header.php'); ?>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>TICKETS</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Tickets</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content" id="tickets-page">
  <div class="container">
    <div id="tickets">
    </div>
  </div><!-- container -->
</section><!-- content -->


<?php include('footer.php'); ?>
<script src="https://js.stripe.com/v3/"></script>
<script src="scripts/tickets.js" charset="utf-8"></script>
