<?php

header('Content-type: application/json');

class DAL {

    private $conn;

    // making a default constructor
    public function __construct() {
        $this->conn = self::dbconnect(); // will call the dbconnect function automatically when the class DAL will be called everytime.
    }

    public function sqlQuery($query, $bindParams) {
        $output = $this->query($query, $bindParams);
        return $output;
    }

    private static function dbconnect() {
        $login = 'greenacre';
        $password = "";

        $dsn = "mysql:host=localhost;dbname=greenacre"; // Set up a DSN for connection with Database Bluecard.

        $opt = array(
            // any occurring errors wil be thrown as PDOException
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            // an SQL command to execute when connecting
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
        );

        // Making a new PDO conenction.
        $conn = new PDO($dsn, $login, $password, $opt);
        // End Connection and Return to other files.

        return $conn; // Returning connection.
    }

    /*
     * -------------------------------------------------------------------------------
     *  This function is called to execute all queries. It will receive two arguments
     *  1. Query String
     *  2. Bind Parameters to execute with the query.
     * -------------------------------------------------------------------------------
     */

    private function query($sql, $bindParams) {
        try {
            $data = $this->conn->prepare($sql); //Prepare SQL query for execution
            $data->execute($bindParams); //Execute query by passing bind parameters
        } catch (Exception $e) {


            die(json_encode(array(
                'error' => $e->getMessage()
                            ), JSON_PRETTY_PRINT)); //Stop execution and print the SQL query error in case query does not execute
        }

        if (strpos($sql, 'SELECT') === false) {  //If SQL query is other then SELECT query then return last insert ID
            if (strstr($sql, 'UPDATE') or strstr($sql, 'DELETE')) {
                return $data->rowCount();
            }
            return $this->conn->lastInsertId();
        }



        $res = $data->fetchAll(PDO::FETCH_ASSOC); //Fetch all the data in case SELECT query is executed

        $results = array();

        foreach ($res as $row) {
            foreach ($row as $k => $v) {
                $result[$k] = $v;
            }
            $results['data'][] = $result;
        }
        if(empty($results)){
          $results['data'] = "";
        }

        // return all the results back to called class.
        return $results;
    }

}

/* End of file config.class.php */
/* Location: ./conf/config.class.php */
