<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>API Test</title>
  </head>
  <body>
    <center>Access Token: c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5 </center>

    <br><hr><br>
    <form class="" action="api.php?choice=search-news" method="post">
      <h3>Search</h3>
      Query: <input type="text" name="query" value="">
      <input type="submit" name="name" value="Search">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=admin-login" method="post">
      <h3>Admin Login</h3>
      email: <input type="text" name="email" value="">
      password: <input type="text" name="password" value="">
      <input type="submit" name="name" value="login">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=admin-update-password" method="post">
      <h3>Update Admin Pass</h3>
      oldpassword: <input type="text" name="oldpassword" value="">
      newpassword: <input type="text" name="newpassword" value="">
      token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="name" value="Update">
    </form>

<!-- ===============================================================================
                                    CRUD USER
    =============================================================================== -->
    <br><hr><br>
    <form class="" action="api.php?choice=register-user" method="POST" enctype="multipart/form-data">
      <h3>Register user</h3>
      <label>Picture:</label>
      <input type="file" id="photo" name="photo" style="margin-bottom:20px">
      <label>Name:</label>
      <input type="text" id="name" name="name" placeholder="Name" class="form-control">
      <label>Age:</label>
      <input type="text" id="age" name="age" placeholder="Age" class="form-control">
      <label>Address:</label>
      <input type="text" id="address" name="address" placeholder="Address" class="form-control">
      <label>Email:</label>
      <input type="text" id="email" name="email" placeholder="Email" class="form-control">
      <label>Password:</label>
      <input type="text" id="password" name="password" placeholder="Password" class="form-control">
      <input type="submit" name="submit" value="Register User">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=login-user" method="post">
      <h3>User Login</h3>
      email: <input type="text" name="email" value="">
      password: <input type="text" name="password" value="">
      <input type="submit" name="name" value="login">
    </form>
<!-- ===============================================================================
                                    CRUD NEWS
    =============================================================================== -->

    <br><hr><br>
    <form class="" action="api.php?choice=view-news" method="POST">
      <h3>View All News</h3>
      <input type="submit" name="submit" value="View News">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-single-news" method="POST">
      <h3>View Single News</h3>
      Slug: <input type="text" name="slug" value="">
      <input type="submit" name="submit" value="View News">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=add-news" method="POST" enctype="multipart/form-data">
      <h3>Add News</h3>
      Image: <input type="file" name="featured" value="">
      Title: <input type="text" name="title" value="">
      Desc:  <input type="text" name="desc" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Add">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=update-news" method="POST" enctype="multipart/form-data">
      <h3>Update News</h3>
      News_id: <input type="text" name="id" value="">
      Image: <input type="file" name="featured" value="">
      Title: <input type="text" name="title" value="">
      Desc:  <input type="text" name="desc" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Update News">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=delete-news" method="POST">
      <h3>Delete News</h3>
      id: <input type="text" name="id" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Delete">
    </form>
<!-- ===============================================================================
                                    CRUD TEAM
    =============================================================================== -->
    <br><hr><br>
    <form class="" action="api.php?choice=add-team" method="POST" enctype="multipart/form-data">
      <h3>Add Team</h3>
      logo: <input type="file" name="logo" value="">
      name: <input type="text" name="name" value="">
      Desc:  <input type="text" name="desc" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Add">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-teams" method="POST">
      <h3>View All Teams</h3>
      <input type="submit" name="submit" value="View Teams">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-single-team" method="POST">
      <h3>View Single Team</h3>
      id: <input type="text" name="id" value="">
      <input type="submit" name="submit" value="View News">
    </form>

<!-- ===============================================================================
                                    CRUD PLAYER
    =============================================================================== -->

    <br><hr><br>
    <form class="" action="api.php?choice=add-player" method="POST" enctype="multipart/form-data">
      <h3>Add Player</h3>
      team_id <input type="text" name="team_id" value="">
      first_name <input type="text" name="first_name" value="">
      last_name <input type="text" name="last_name" value="">
      squad_no <input type="text" name="squad_no" value="">
      position <input type="text" name="position" value="">
      dob <input type="text" name="dob" value="">
      age <input type="text" name="age" value="">
      desc <input type="text" name="desc" value="">
      IMAGE: <input type="file" name="image" value="">
      Featured IMAGE: <input type="file" name="featured" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Add">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-all-players" method="POST">
      <h3>View All Players </h3>
      <input type="submit" name="submit" value="Get Players">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-player-by-team" method="POST">
      <h3>View Players by Team id</h3>
      id: <input type="text" name="id" value="">
      <input type="submit" name="submit" value="Get Players">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-single-player" method="POST">
      <h3>View Single Player</h3>
      id: <input type="text" name="id" value="">
      <input type="submit" name="submit" value="Get Player Detail">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=delete-player" method="POST">
      <h3>Delete Player</h3>
      id: <input type="text" name="id" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Delete">
    </form>


    <br><hr><br>
    <form class="" action="api.php?choice=create-fixture" method="POST">
      <h3>Add Fixture</h3>
      teamA_id <input type="text" name="teamA_id" value="">
      teamB_id <input type="text" name="teamB_id" value="">
      date <input type="date" name="date" value="">
      location <input type="text" name="location" value="">
      tickets_enabled <input type="text" name="tickets_enabled" value="">
      Tickets Count: <input type="text" name="no_of_tickets" value="">
      Price <input type="text" name="price" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Add">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-fixtures" method="POST">
      <h3>View All Fixtures</h3>
      <input type="submit" name="submit" value="View Fixtures">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=update-result                " method="POST">
      <h3>Add Fixture</h3>
      fixture_id <input type="text" name="fixture_id" value="">
      teamA_score <input type="text" name="teamA_score" value="">
      teamB_score <input type="text" name="teamB_score" value="">
      Access_token: <input type="text" name="access_token" value="c2hpdmFwYW5kZXlzaGl2YXBhbmRleXNoaXZhcGFuZGV5">
      <input type="submit" name="submit" value="Update Result">
    </form>

    <br><hr><br>
    <form class="" action="api.php?choice=view-single-fixture" method="POST">
      <h3>View Single Fixture</h3>
      id: <input type="text" name="id" value="">
      <input type="submit" name="submit" value="Get Fixture Detail">
    </form>

    <br><br><br><br>

  </body>
</html>
