<?php

header('Content-type: application/json');

class News {
  private $_DAL;
  private $_FUN;

  public function __autoload($classname){
    require $classname.'.class.php';
  }

  public function __construct() {
    $this->_DAL = new DAL;
    $this->_FUN = new Functions;
  }
  public function addNews($title,$desc,$token,$image){
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
  if($verify['data'] != "") {
      $date = date("Y-m-d");
      $premalink = date("Y/m/d").time();
      $premalink = base64_encode($premalink);
      $premalink = $title.$premalink;
      $premalink = $this->_FUN->clean($premalink);
      $query = "INSERT INTO news (title, description, slug, featured_image, admin_id) VALUES (?,?,?,?,?)";
      $value = array($title, $desc, $premalink, $image, $verify['data'][0]['admin_id']);
      $data = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "News Added Successfully", "data"=> "");
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

  public function updateNews($title,$desc,$token,$image, $id){
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
  if($verify['data'] != "") {
      $premalink = date("Y/m/d").time();
      $premalink = base64_encode($premalink);
      $premalink = $title."-".$premalink;
      $premalink = $this->_FUN->clean($premalink);
      $date = date("Y-m-d");
      if($image !== "false"){
        $query = "UPDATE news SET title = ?, description = ?, slug = ?, featured_image = ? WHERE news_id = ?";
        $value = array($title, $desc, $premalink, $image, $id);
      } else {
        $query = "UPDATE news SET title = ?, description = ?, slug = ? WHERE news_id = ?";
        $value = array($title, $desc, $premalink, $id);
      }
      $data = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "News Updated Successfully", "data"=> "");
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

  public function viewNews() {
    $query = "SELECT * FROM news ORDER BY news_id DESC";
    $value = array();
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function viewSingleNews($premalink) {
    $query = "SELECT * FROM news WHERE slug = ?";
    $value = array($premalink);
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response);
    return $json;
  }

  public function deleteNews($id,$token) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
    $count = count($verify);
    if($count != 0) {
      $query = "DELETE FROM news WHERE news_id = ?";
      $value = array($id);
      $response = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "News Deleted Successfully", "data"=> $response);
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

  public function searchNews($user_query) {
    $query = "SELECT * FROM news WHERE MATCH(title, description) AGAINST(? IN NATURAL LANGUAGE MODE)";
    $value = array($user_query);
    $response = $this->_DAL->sqlQuery($query, $value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function addFeaturedImage() {
    $hash = md5(date("Y/m/d").time());
    $target = "../img/news/".$hash.basename($_FILES["featured"]["name"]);
    $target1 = "img/news/".$hash.basename($_FILES["featured"]["name"]);
    $uploadOk = 1;
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["featured"]["tmp_name"]);
    if ($check !== false) {
      $uploadOk = 1;
    } else {
      $uploadOk = 0;
      return array("status" => 0, "message"=>"Please Upload Image file");
    }
    // Check file size
    if ($_FILES["featured"]["size"] > 5000000) {
      $uploadOk = 0;
      return array("status" => 0, "message"=>"Image File Too Large");
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      return array("status" => 0, "message"=>"Upload Failed");
      // Uploading file
    } else {
      if (move_uploaded_file($_FILES["featured"]["tmp_name"], $target)) {
        $data = array("status" => 1, "message"=>"Uploaded Successfully", "url" => $target1,);
        return $data;
      } else {
        return array("status" => 0, "message"=>"Upload Failed");
      }
    }
  } // featured image

}// class
