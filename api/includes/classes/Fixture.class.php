<?php

header('Content-type: application/json');

class Fixture {
  private $_DAL;
  private $_FUN;

  public function __autoload($classname){
    require $classname.'.class.php';
  }

  public function __construct() {
    $this->_DAL = new DAL;
    $this->_FUN = new Functions;
  }

  // $_POST['teamA_id'], $_POST['teamB_id'], $_POST['date'], $_POST['location'], $_POST['tickets_enabled'], $_POST['access_token']

  public function createFixture($teamA_id, $teamB_id, $date, $location, $tickets_enabled, $access_token, $no_of_tickets, $price, $time) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($access_token);
    $verify = $this->_DAL->sqlQuery($query,$value);
    if($verify['data'] != "") {
      if(!$tickets_enabled) {
        $query = "INSERT INTO fixture_result (teamA_id, teamB_id, `date`,location, tickets_enabled, result, `time` ) VALUES (?,?,?,?,?,?,?)";
        $value = array($teamA_id, $teamB_id, $date, $location, $tickets_enabled, 0, $time);
      } else {
        $query = "INSERT INTO fixture_result (teamA_id, teamB_id, `date`,location, tickets_enabled, price, tickets_count, result, `time` ) VALUES (?,?,?,?,?,?,?,?,?)";
        $value = array($teamA_id, $teamB_id, $date, $location, $tickets_enabled, $price, $no_of_tickets, 0, $time);
      }
      $data = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Fixture Added Successfully", "data"=> "");
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

  public function viewAllFixtures() {
    $query = "SELECT t1.name AS teamA, t1.logo AS teamA_logo, t2.name AS teamB, t2.logo AS teamB_logo, i.* FROM fixture_result i JOIN team t1 ON t1.team_id = i.teamA_id JOIN team t2 ON t2.team_id = i.teamB_id ORDER BY fixture_result_id DESC";
    $value = array();
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function viewSingleFixture($id) {
    $query = "SELECT t1.name AS teamA, t1.logo AS teamA_logo, t2.name AS teamB, t2.logo AS teamB_logo, i.* FROM fixture_result i JOIN team t1 ON t1.team_id = i.teamA_id JOIN team t2 ON t2.team_id = i.teamB_id WHERE fixture_result_id = ?";
    $value = array($id);
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data'][0]);
    return $json;
  }


  public function updateResult($id, $teamA_score, $teamB_score,$token) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
  if($verify['data'] != "") {
      $query = "UPDATE fixture_result SET teamA_score = ?, teamB_score = ?, result = ?, tickets_enabled = ? WHERE fixture_result_id = ?";
      $value = array($teamA_score, $teamB_score, 1, 0, $id);
      $data = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Result Updated Successfully", "data"=> "");
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
    $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
    return $json;
  }

  public function deleteFixture($id,$token) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
    $count = count($verify);
    if($count != 0) {
      $query = "DELETE FROM fixture_result WHERE fixture_result_id = ?";
      $value = array($id);
      $response = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Fixture Deleted Successfully", "data"=> $response);
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

  public function chargeUser($ticket_id, $charge_id, $user_id) {
    $query = "SELECT * FROM user WHERE user_id = ?";
    $value = array($user_id);
    $user = $this->_DAL->sqlQuery($query,$value);
    if(count($user) != 0) {
      $ticket = $this->viewSingleFixture($ticket_id);
      $user = $user['data'][0];
      $ticket = $ticket['data'];
      $description = " Name: {$user['name']}, Match: {$ticket['teamA']} vs {$ticket['teamB']}, Date: {$ticket['date']}   ";
      $price = $ticket['price'] * 100;
      $json = $this->createCharge($charge_id, $price, $description);
      if($json['status']){
        $query = "INSERT INTO user_ticket (ticket_id, user_id) VALUES (?,?)";
        $value = array($ticket_id, $user_id);
        $response = $this->_DAL->sqlQuery($query,$value);
        $mail = $this->_FUN->send_email($user['email'],"Ticket Purchased",$this->createTicket($ticket, $user));
        $json = array("status" => 1, "message"=> $json['message'], "data"=> $response);
      }
    } else {
      $json = array("status" => 0, "message"=> "User Not Authorized", "data"=> "");
    }

    return $json;
  }

  public function createTicket($ticket, $user) {
    $str = '<h3>You have successfully purchased Ticket</h3>';
    $str .="<!DOCTYPE html>\n";
    $str .="<html lang=\"en\">\n";
    $str .="<body>\n";
    $str .="  <!--top section  -->\n";
    $str .="  <div style=\"width:750px;height:248px;background:#f9f1f1;font-family: sans-serif;\">\n";
    $str .="    <div style=\"text-align:center;height:75px;background: url(https://s3.envato.com/files/88880472/Image_Preview.jpg) no-repeat center center;background-size: cover;\">\n";
    $str .="      <div style=\"  background-image: linear-gradient(to right, #9e8383 0%, #637379 100%);\n";
    $str .="      opacity: 0.9;\n";
    $str .="      height: 75px;\">\n";
    $str .="    <h2 style=\"letter-spacing: 10px;\n";
    $str .="    padding-top: 7px;\n";
    $str .="    z-index: 50;\n";
    $str .="    color: white;\">THE <br> PERFECT MATCH</h2>\n";
    $str .="    </div>\n";
    $str .="    <div style=\"opacity: 0.3;\"> </div>\n";
    $str .="    <div style=\"display:flex;height:200px;background-image: linear-gradient(-20deg, #e9defa 0%, #fbfcdb 100%);\n";
    $str .="    background-size: cover;\n";
    $str .="    \">\n";
    $str .="    <div  style=\"width:500px;\">\n";
    $str .="      <h1 style=\"margin-top: 24px;margin-bottom: 0px;\">".$ticket['teamA']." <span>vs</span> ".$ticket['teamB']."</h1>\n";
    $str .="      <h2 style=\"margin-top: 13px;margin-bottom: 0px;\">".$ticket['date']." </h2>\n";
    $str .="      <h3 style=\"margin-top: 13px;margin-bottom: 0px;\">".$ticket['location']."</h3>\n";
    $str .="      <h2 style=\"margin-top: 13px;margin-bottom: 0px;\">KICK OFF ".$ticket['time']."</h2>\n";
    $str .="    </div>\n";
    $str .="    <!-- left -->\n";
    $str .="    <div style=\"width:250px;border-left: 2px dashed #9a9494;\">\n";
    $str .="      <h1 style=\"font-size:27px;margin-top:24px;\">".$user['name']."</h1>\n";
    $str .="      <h3 style=\"\">Price</h3>\n";
    $str .="      <p style=\"font-size: 65px;\n";
    $str .="      margin: 0px;\n";
    $str .="      font-style: italic;\">$".$ticket['price']."</p>\n";
    $str .="\n";
    $str .="    </div>\n";
    $str .="  </div>\n";
    $str .="  <!-- right start -->\n";
    $str .="</div>\n";
    $str .="\n";
    $str .="<!-- flex-container- -->\n";
    $str .="\n";
    $str .="</div><!--header  -->\n";
    $str .="<div>\n";
    $str .="  <p style=\"box-sizing: border-box;\n";
    $str .="  margin-top: 27px;\n";
    $str .="  background-image: linear-gradient(to right, #9e8383 0%, #637379 100%);\n";
    $str .="  padding: 20px;\n";
    $str .="  width: 750px;\n";
    $str .="  text-align: center;color:white;\">PLEASE TAKE YOUR SEATS 30 MINUTES BEFORE THE START OF THE MATCH</p>\n";
    $str .="\n";
    $str .="</div>\n";
    $str .="</div><!--container  -->\n";
    $str .="\n";
    $str .="</body>\n";
    $str .="</html>\n";
    return $str;
  }

  public function createCharge ($charge_id, $price, $description) {
    require 'stripe/Stripe.php';
    $params = array(
    	"testmode"   => "on",
    	"private_live_key" => "sk_live_xxxxxxxxxxxxxxxxxxxxx",
    	"public_live_key"  => "pk_live_xxxxxxxxxxxxxxxxxxxxx",
    	"private_test_key" => "sk_test_UPUvGuYaj9pCftEiNTsJJtE7",
    	"public_test_key"  => "pk_test_kUKu2CpsMYlUAJG6GwQW9Z9y"
    );

    if ($params['testmode'] == "on") {
    	Stripe::setApiKey($params['private_test_key']);
    	$pubkey = $params['public_test_key'];
    } else {
    	Stripe::setApiKey($params['private_live_key']);
    	$pubkey = $params['public_live_key'];
    }

  	$amount_cents = str_replace(".","",$price);  // Chargeble amount

  	try {

  		$charge = Stripe_Charge::create(array(
  			  "amount" => $amount_cents,
  			  "currency" => "usd",
  			  "source" => $charge_id,
  			  "description" => $description)
  		);

  		if ($charge->card->address_zip_check == "fail") {
  			throw new Exception("zip_check_invalid");
  		} else if ($charge->card->address_line1_check == "fail") {
  			throw new Exception("address_check_invalid");
  		} else if ($charge->card->cvc_check == "fail") {
  			throw new Exception("cvc_check_invalid");
  		}
  		// Payment has succeeded, no exceptions were thrown or otherwise caught

  		$result = "success";

  	} catch(Stripe_CardError $e) {

  	$error = $e->getMessage();
  		$result = "declined";

  	} catch (Stripe_InvalidRequestError $e) {
  		$result = "declined";
  	} catch (Stripe_AuthenticationError $e) {
  		$result = "declined";
  	} catch (Stripe_ApiConnectionError $e) {
  		$result = "declined";
  	} catch (Stripe_Error $e) {
  		$result = "declined";
  	} catch (Exception $e) {

  		if ($e->getMessage() == "zip_check_invalid") {
  			$result = "declined";
  		} else if ($e->getMessage() == "address_check_invalid") {
  			$result = "declined";
  		} else if ($e->getMessage() == "cvc_check_invalid") {
  			$result = "declined";
  		} else {
  			$result = "declined";
  		}
  	}    
    if($result == "declined") {
      $json = array("status" => 0, "message"=> "Card Declined", "data"=> "");
    } else {
      $json = array("status" => 1, "message"=> "Payment Successful!", "data"=> "");
    }
    return $json;
  }
}// class
