<?php
/**
 * @ Description : UserProfile.class.php contains class UserProfile.
 * It include all the functions related to UserProfile.
 * @ Developed by : 75way Technologies Pvt. Ltd.
 */
 if (isset($_SERVER['HTTP_ORIGIN'])) {
   header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
   header('Access-Control-Allow-Credentials: true');
   header('Access-Control-Max-Age: 86400');    // cache for 1 day
 }

 // Access-Control headers are received during OPTIONS requests
 if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

   if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
   header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

   if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
   header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

   exit(0);
 }


header('Content-type: application/json');

class AdminDetails {
    private $_dalObj;
    private $_funcObj;

    public function __autoload($classname){
        require $classname.'class.php';
    }

    public function __construct(){
        $this->_dalObj = new DAL;
        $this->_funcObj = new Functions;
    }

    public function login($email1,$password){
        $query = "SELECT * FROM admin WHERE email= ? LIMIT 1";
        $value = array($email1);
        $email = $this->_dalObj->sqlQuery($query,$value);
        if(isset($email['data'][0]['email'])== $email1){
            $password = base64_encode($password);
            if($password == $email['data'][0]['password']){
                $data = array("access_token" => $email['data'][0]['access_token'],"id" => $email['data'][0]['admin_id']);
                $json = array("status" => 1, "message"=> "Login Successful", "data"=> $data);
                return $json;
            } else {
                $json = array("status" => 0, "message"=> "Password Incorrect", "data"=> "");
                return $json;
            }
        } else {
            return array("status" => 0, "message" => "Email Address Not Registered With Us!", "data" => "");
        }
    }

    public function updatePassword($oldpassword, $newpassword, $token){
      $query = "SELECT * FROM admin WHERE access_token= ? LIMIT 1";
      $value = array($token);
      $verifyUser = $this->_dalObj->sqlQuery($query,$value);
      if($verifyUser['data'] != "" ) {
        $password = base64_encode($oldpassword);
        if($verifyUser['data'][0]['password'] != $password){
          $json = array("status"=>0, "message"=>"Old Password Incorrect", "data"=>'');
          return $json;
        }
        $password = base64_encode($newpassword);
        $query = "UPDATE admin SET password = ? WHERE access_token = ?";
        $value = array($password,$token);
        $data = $this->_dalObj->sqlQuery($query,$value);
        // $mail = $this->_funcObj->send_email($verifyUser['data'][0]['email'],"Password Updated","Your Password has been updated successfully!");
        $json = array("status"=>1, "message"=>"Update Successful!", "data"=>'');
      } else {
        $json = array("status"=>0, "message"=>"Cannot Update Password", "data"=>"");
      }
      return $json;
   }


}// Class


?>
