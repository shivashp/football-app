<?php
header('Content-type: application/json');

class Team {
  private $_DAL;
  private $_FUN;

  public function __autoload($classname){
    require $classname.'.class.php';
  }

  public function __construct() {
    $this->_DAL = new DAL;
    $this->_FUN = new Functions;
  }

  public function addTeam($name,$desc,$token,$image){
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
  if($verify['data'] != "") {
      $query = "INSERT INTO team (name, team_description, logo) VALUES (?,?,?)";
      $value = array($name, $desc, $image);
      $data = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Team Added Successfully", "data"=> "");
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
    $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
    return $json;
  }


  public function viewTeams() {
    $query = "SELECT * FROM team ORDER BY team_id DESC";
    $value = array();
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function viewSingleTeam($id) {
    $query = "SELECT * FROM team WHERE team_id = ?";
    $value = array($id);
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function deleteTeam($id,$token) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
    $count = count($verify);
    if($count != 0) {
      $query = "DELETE FROM team WHERE team_id = ?";
      $value = array($id);
      $response = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Team Deleted Successfully", "data"=> $response);
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

  public function addPlayer($team_id, $first_name, $last_name, $squad, $position, $dob, $age, $desc, $token, $image, $featured) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
  if($verify['data'] != "") {
      $query = "INSERT INTO player (team_id, first_name, last_name, squad_no, image, featured_image, position, dob, age, description) VALUES (?,?,?,?,?,?,?,?,?,?)";
      $value = array($team_id, $first_name, $last_name, $squad, $image, $featured, $position, $dob, $age, $desc);
      $data = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Player Added Successfully", "data"=> "");
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
    $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
    return $json;
  }

  public function viewAllPlayers() {
    $query = "SELECT * FROM player INNER JOIN team ON player.team_id = team.team_id";
    $value = array();
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function viewTeamPlayers($id) {
    $query = "SELECT * FROM player WHERE team_id = ?";
    $value = array($id);
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function viewSinglePlayer($id) {
    $query = "SELECT * FROM player INNER JOIN team ON player.team_id = team.team_id WHERE player_id = ?";
    $value = array($id);
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function deletePlayer($id, $token) {
    $query = "SELECT * FROM admin WHERE access_token = ?";
    $value = array($token);
    $verify = $this->_DAL->sqlQuery($query,$value);
    $count = count($verify);
    if($count != 0) {
      $query = "DELETE FROM player WHERE player_id = ?";
      $value = array($id);
      $response = $this->_DAL->sqlQuery($query,$value);
      $json = array("status" => 1, "message"=> "Player Deleted Successfully", "data"=> $response);
      return $json;
    } else {
      $json = array("status" => 0, "message"=> "Access Token Incorrect", "data"=> "");
      return $json;
    }
  }

}// class
