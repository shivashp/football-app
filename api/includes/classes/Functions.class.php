<?php
// header('Access-Control-Allow-Origin: *');
/**
* @ Description : Functions.class.php contains class Functions.
* It include all the functions.
* @ Developed by : 75way Technologies Pvt. Ltd.
*/
session_start();
header('Content-type: application/json');

class Functions {

  private $_dalObj;
  //  private $_s3;

  public function __construct() {
    $this->_dalObj = new DAL;
  }

  function __autoload($class_name) {
    require $class_name . '.class.php';
  }

  /*=================================================================
  *  Send Mail function i.e to send new password to the mail ..
  *==================================================================
  */

    public function send_email ($to,$subject,$body){
      require 'mail/PHPMailerAutoload.php';
      $mail = new PHPMailer;
      $mail->isSMTP();
      $mail->Host = 'tls://smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->SMTPKeepAlive = true; // SMTP connection will not close after each email sent, reduces SMTP overhead
      $mail->Port = 465;
      $mail->Username = 'freakoidsmailer@gmail.com';
      $mail->Password = 'Freakoids@123';
      $mail->setFrom('info@greenacre.com', 'Greenacre');
      $mail->addReplyTo($to, 'Greenacre');
      $mail->Subject = $subject;
      $mail->msgHTML($body);
      $mail->addAddress($to, "");
      if ($mail->send()) {
          return true;
      } else {
          return false;
      }
    }

    public function clean($string) {
      $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
      return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function verifyAdmin($token) {
      $query = "SELECT * FROM login WHERE access_token = ? AND type= ?";
      $value = array($token, "admin");
      $verify = $this->_dalObj->sqlQuery($query,$value);
      return $verify;
    }

    public function verifySubAdmin($token) {
      $query = "SELECT * FROM login WHERE access_token = ? AND type= ?";
      $value = array($token, "sub-admin");
      $verify = $this->_dalObj->sqlQuery($query,$value);
      return $verify;
    }

    public function verifyUser($token) {
      $query = "SELECT * FROM user WHERE access_token = ?";
      $value = array($token);
      $verify = $this->_dalObj->sqlQuery($query,$value);
      return $verify;
    }

    public function createPremalink($title) {

      $premalink = date("Y/m/d").time();
      $premalink = base64_encode($premalink);
      $premalink = $title.$premalink;
      $premalink = clean($premalink);
      return $premalink;
    }

    public function pro_cons($data){
      for($i = 0; $i<4;$i++){
        $data[$i] = isset($data[$i]) ? $data[$i] : "";
      }
      return $data;
    }

    public function uploadImage($file, $location){
      $hash = md5(date("Y/m/d").time());
      $target = "img/".$location.$hash.basename($_FILES[$file]["name"]);
      $uploadOk = 1;

      // Check file size
      if ($_FILES[$file]["size"] > 5000000) {
        $uploadOk = 0;
        return array("status" => 0, "message"=>"Image File Too Large");
      }
        if (move_uploaded_file($_FILES[$file]["tmp_name"], "../".$target)) {
          $data = array("status" => 1, "message"=>"Uploaded Successfully", "image" => $target);
          return $data;
        } else {
          return array("status" => 0, "message"=>"Image Upload Failed", "image" => "");
        }
    }

}
