<?php

header('Content-type: application/json');

class Users {
  private $_DAL;
  private $_FUN;

  public function __autoload($classname) {
    require $classname.'.class.php';
  }

  public function __construct() {
    $this->_DAL = new DAL;
    $this->_FUN = new Functions;
  }

  public function registerUser($name, $age, $address, $email, $password, $image) {
    $query = "SELECT * FROM user WHERE email= ? LIMIT 1";
    $value = array($email);
    $data = $this->_DAL->sqlQuery($query,$value);
    if(isset($data['data'][0]['email']) == $email){
      return array("status" => 0, "message" => "Email Address Already Registered With Us!", "data" => "");
    }
    $password = md5($password);
    $access_token = md5($email.$password.$email.time());
    $query = "INSERT INTO user (name, age, address, photo, email, password, access_token) VALUES (?,?,?,?,?,?,?)";
    $value = array($name, $age, $address, $image, $email, $password, $access_token);
    $data = $this->_DAL->sqlQuery($query,$value);
    $mail = $this->_FUN->send_email($email,"Greenacre Registration","Your have been successfully registered!");
    $json = array("status" => 1, "message"=> "User Registered Successfully", "data"=> $mail);
    return $json;
  }

  public function login($email1,$password){
      $query = "SELECT * FROM user WHERE email= ? LIMIT 1";
      $value = array($email1);
      $email = $this->_DAL->sqlQuery($query,$value);
      if(isset($email['data'][0]['email'])== $email1){
          $password = md5($password);
          if($password == $email['data'][0]['password']){
              $data = array("access_token" => $email['data'][0]['access_token'],"user_id" => $email['data'][0]['user_id'],"name" => $email['data'][0]['name']);
              $json = array("status" => 1, "message"=> "Login Successful", "data"=> $data);
              return $json;
          } else {
              $json = array("status" => 0, "message"=> "Password Incorrect", "data"=> "");
              return $json;
          }
      } else {
          return array("status" => 0, "message" => "Email Address Not Registered With Us!", "data" => "");
      }
  }

  public function subscribe($email) {
    $query = "SELECT * FROM subscription WHERE email= ? LIMIT 1";
    $value = array($email);
    $data = $this->_DAL->sqlQuery($query,$value);
    if(isset($data['data'][0]['email']) == $email){
      return array("status" => 0, "message" => "You have Already Subscribed!", "data" => "");
    }
    $query = "INSERT INTO subscription (email) VALUES (?)";
    $value = array($email);
    $data = $this->_DAL->sqlQuery($query,$value);
    $mail = $this->_FUN->send_email($email,"Subscription to Greenacre Successful","Your have been successfully subscribed to our newsletter!");
    $json = array("status" => 1, "message"=> "Subscribed Successfully", "data"=> $mail);
    return $json;
  }

  public function contact($name, $message, $email) {
    $str = '';
    $str .= '<h1 style="text-align: center">Message from Greenacre</h1><br><br>';
    $str .= '<b>Name:</b> '.$name.'<br>';
    $str .= '<b>Email:</b> '.$email.'<br>';
    $str .= '<b>Message:</b> '.$message;
    $mail = $this->_FUN->send_email("shivapandey2022@gmail.com","Greenacre | Message Received",$str);
    if($mail) {
      $json = array("status" => 1, "message"=> "Message Sent", "data"=> $mail);
    } else {
      $json = array("status" => 0, "message"=> "Message Couldn't be send", "data"=> $mail);
    }
    return $json;
  }

  public function viewAllUsers() {
    $query = "SELECT * FROM user ORDER BY user_id DESC";
    $value = array();
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }

  public function viewSingleUser($id) {
    $query = "SELECT * FROM user WHERE user_id = ?";
    $value = array($id);
    $response = $this->_DAL->sqlQuery($query,$value);
    $json = array("status" => 1, "message"=> "", "data"=> $response['data'][0]);
    return $json;
  }

  public function viewUserTickets($id){
    $query = "SELECT * FROM user_ticket JOIN fixture_result ON user_ticket.ticket_id = fixture_result.fixture_result_id WHERE user_ticket.user_id = ?";
    $value = array($id);
    $response = $this->_DAL->sqlQuery($query,$value);
    $fixObj = new Fixture;
    if($response['data'] !== ''){
      for ($i=0; $i < count($response['data']); $i++) {
        $id = $response['data'][$i]['ticket_id'];
        $fixture = $fixObj->viewSingleFixture($id);
        $response['data'][$i]['teamA'] = $fixture['data']['teamA'];
        $response['data'][$i]['teamB'] = $fixture['data']['teamB'];
      }
    }
    $json = array("status" => 1, "message"=> "", "data"=> $response['data']);
    return $json;
  }



}// class
