<?php
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
  header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
  header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

  exit(0);
}

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('html_errors', false);
/**
* @ Description : This file will receive the action in GET method.
* And then the corresponding class will be called according to $_GET['action'].
*/
header('Content-type: application/json');
require 'conf/DAL.class.php';

function __autoload($class_name) {
  include "includes/classes/" . $class_name . ".class.php";
}

try {
  $postInput = array();
  $json = array("status" => 0, "message" => "Resource Not Found");
  // Perform the corresponding action based on the choice
  switch (isset($_GET['choice']) ? $_GET['choice'] : "false") {
    case 'admin-login':
      array_push($postInput, $_POST['email'], $_POST['password']);
      emptyCheck($postInput);
      $obj = new AdminDetails;
      $json = $obj->login($_POST['email'], $_POST['password']);
    break;

    case 'admin-update-password':
      array_push($postInput, $_POST['oldpassword'], $_POST['newpassword'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new AdminDetails;
      $json = $obj->updatePassword($_POST['oldpassword'],$_POST['newpassword'], $_POST['access_token']);
    break;

    case 'search-news':
      array_push($postInput, $_POST['query']);
      emptyCheck($postInput);
      $obj = new News;
      $json = $obj->searchNews($_POST['query']);
    break;

/* --------------------------------------------------------------------------
**                            CRUD USER
** -------------------------------------------------------------------------- */
  case 'register-user':
    array_push($postInput, $_POST['name'], $_POST['age'], $_POST['address'], $_POST['email'], $_POST['password']);
    emptyCheck($postInput);
    $obj = new Users;
    $funcObj = new Functions;
    if($_FILES['photo']['name'] !== '') {
      $json = $funcObj->uploadImage('photo', 'users/');
      if(!$json['status']) {
        break;
      }
      $image = $json['image'];
    } else {
      $json = array("status" => 0, "message"=> "User Photo Missing", "data"=> "");
      break;
    }
    $json = $obj->registerUser($_POST['name'], $_POST['age'], $_POST['address'], $_POST['email'], $_POST['password'],$image);
  break;

  case 'login-user':
    array_push($postInput, $_POST['email'], $_POST['password']);
    emptyCheck($postInput);
    $obj = new Users;
    $json = $obj->login($_POST['email'], $_POST['password']);
  break;

  case 'subscribe':
    array_push($postInput, $_POST['email']);
    emptyCheck($postInput);
    $obj = new Users;
    $json = $obj->subscribe($_POST['email']);
  break;

  case 'contact':
  array_push($postInput, $_POST['name'], $_POST['message'], $_POST['email']);
  emptyCheck($postInput);
  $obj = new Users;
  $json = $obj->contact($_POST['name'], $_POST['message'], $_POST['email']);
  break;

  case 'view-single-user':
    array_push($postInput, $_POST['id']);
    emptyCheck($postInput);
    $obj = new Users;
    $json = $obj->viewSingleUser($_POST['id']);
  break;

  case 'view-user-tickets':
    array_push($postInput, $_POST['id']);
    emptyCheck($postInput);
    $obj = new Users;
    $json = $obj->viewUserTickets($_POST['id']);
  break;



/* --------------------------------------------------------------------------
**                            CRUD NEWS
** -------------------------------------------------------------------------- */

    case 'add-news':
      array_push($postInput, $_POST['title'], $_POST['desc'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new News;
      if($_FILES['featured']['name'] !== '') {
        $json = $obj->addFeaturedImage();
        if(!$json['status']) {
          break;
        }
        $image = $json['url'];
      } else {
        $image = 'img/news/default-featured.png';
      }
      $json = $obj->addNews($_POST['title'],$_POST['desc'], $_POST['access_token'],$image);
    break;

    case 'update-news':
      array_push($postInput, $_POST['title'], $_POST['desc'], $_POST['access_token'], $_POST['id']);
      emptyCheck($postInput);
      $obj = new News;
      if($_FILES['featured']['name'] !== '') {
        $json = $obj->addFeaturedImage();
        if(!$json['status']) {
          break;
        }
        $image = $json['url'];
      } else {
        $image = 'false';
      }
      $json = $obj->updateNews($_POST['title'],$_POST['desc'], $_POST['access_token'],$image, $_POST['id']);
    break;

    case 'view-news':
      $obj = new News;
      $json = $obj->viewNews();
    break;

    case 'view-single-news':
      array_push($postInput, $_POST['slug']);
      emptyCheck($postInput);
      $obj = new News;
      $json = $obj->viewSingleNews($_POST['slug']);
    break;

    case 'delete-news':
      array_push($postInput, $_POST['id'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new News;
      $json = $obj->deleteNews($_POST['id'], $_POST['access_token']);
    break;

/* --------------------------------------------------------------------------
**                            CRUD TEAM
** -------------------------------------------------------------------------- */
    case 'add-team':
      array_push($postInput, $_POST['name'], $_POST['desc'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new Team;
      $funcObj = new Functions;
      if($_FILES['logo']['name'] !== '') {
        $json = $funcObj->uploadImage('logo', 'teams/');
        if(!$json['status']) {
          break;
        }
        $image = $json['image'];
      } else {
        $json = array("status" => 0, "message"=> "Team Logo Missing", "data"=> "");
        break;
      }
      $json = $obj->addTeam($_POST['name'], $_POST['desc'], $_POST['access_token'], $image);
    break;

    case 'view-teams':
      $obj = new Team;
      $json = $obj->viewTeams();
    break;

    case 'view-single-team':
      array_push($postInput, $_POST['id']);
      emptyCheck($postInput);
      $obj = new Team;
      $json = $obj->viewSingleTeam($_POST['id']);
    break;

    case 'delete-team':
      array_push($postInput, $_POST['id'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new Team;
      $json = $obj->deleteTeam($_POST['id'], $_POST['access_token']);
    break;

/* --------------------------------------------------------------------------
**                            CRUD Player
** -------------------------------------------------------------------------- */
    case 'add-player':
      array_push($postInput, $_POST['team_id'], $_POST['first_name'], $_POST['last_name'], $_POST['squad_no'], $_POST['position'], $_POST['dob'], $_POST['age'], $_POST['desc'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new Team;
      $funcObj = new Functions;
      if($_FILES['image']['name'] !== '' && $_FILES['featured']['name'] !== '') {
        $json = $funcObj->uploadImage('image', 'teams/players/');
        $json2 = $funcObj->uploadImage('featured', 'teams/players/');
        if(!$json['status']) {
          break;
        }
        if(!$json2['status']) {
          $json = $json2;
          break;
        }
        $image = $json['image'];
        $featured_image = $json2['image'];
      } else {
        $json = array("status" => 0, "message"=> "Team Logo Missing", "data"=> "");
        break;
      }
      $json = $obj->addPlayer($_POST['team_id'], $_POST['first_name'], $_POST['last_name'], $_POST['squad_no'], $_POST['position'], $_POST['dob'], $_POST['age'], $_POST['desc'], $_POST['access_token'], $image, $featured_image);
    break;

    case 'view-all-players':
      $obj = new Team;
      $json = $obj->viewAllPlayers();
    break;

    case 'view-player-by-team':
      array_push($postInput, $_POST['id']);
      emptyCheck($postInput);
      $obj = new Team;
      $json = $obj->viewTeamPlayers($_POST['id']);
    break;

    case 'view-single-player':
      array_push($postInput, $_POST['id']);
      emptyCheck($postInput);
      $obj = new Team;
      $json = $obj->viewSinglePlayer($_POST['id']);
    break;

    case 'delete-player':
      array_push($postInput, $_POST['id'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new Team;
      $json = $obj->deletePlayer($_POST['id'], $_POST['access_token']);
    break;

/* --------------------------------------------------------------------------
**                            CRUD Fixtre Result
** -------------------------------------------------------------------------- */

    case 'create-fixture':
      array_push($postInput, $_POST['teamA_id'], $_POST['teamB_id'], $_POST['date'], $_POST['location'], $_POST['tickets_enabled'], $_POST['access_token'], $_POST['no_of_tickets'], $_POST['price'], $_POST['time']);
      emptyCheck($postInput);
      $obj = new Fixture;
      $json = $obj->createFixture($_POST['teamA_id'], $_POST['teamB_id'], $_POST['date'], $_POST['location'], $_POST['tickets_enabled'], $_POST['access_token'],  $_POST['no_of_tickets'], $_POST['price'], $_POST['time']);
    break;

    case 'view-fixtures':
      $obj = new Fixture;
      $json = $obj->viewAllFixtures();
    break;

    case 'view-single-fixture':
      array_push($postInput, $_POST['id']);
      emptyCheck($postInput);
      $obj = new Fixture;
      $json = $obj->viewSingleFixture($_POST['id']);
    break;

    case 'update-result':
      array_push($postInput, $_POST['fixture_id'], $_POST['teamA_score'], $_POST['teamB_score'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new Fixture;
      $json = $obj->updateResult($_POST['fixture_id'], $_POST['teamA_score'], $_POST['teamB_score'], $_POST['access_token']);
    break;

    case 'delete-fixture':
      array_push($postInput, $_POST['id'], $_POST['access_token']);
      emptyCheck($postInput);
      $obj = new Fixture;
      $json = $obj->deleteFixture($_POST['id'], $_POST['access_token']);
    break;

    case 'charge-user':
      array_push($postInput, $_POST['ticket_id'], $_POST['charge_id'], $_POST['user_id']);
      emptyCheck($postInput);
      $obj = new Fixture;
      $json = $obj->chargeUser($_POST['ticket_id'], $_POST['charge_id'], $_POST['user_id']);
    break;



    throw new Exception('Wrong action !');
    break;
  }
} catch (Exception $ex) {

}

// Check if any data is empty in post
function emptyCheck($data) {
  if (array_search("", $data) !== false) {
    die(json_encode(array("status" => 0, "message" => "Some Parameters Missing!")));
  }
}

echo json_encode($json);
?>
