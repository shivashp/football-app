<?php include('header.php'); ?>

<script>
  var player_id = "<?php echo $_GET['id']; ?>";
  if(player_id == null || player_id == undefined || player_id == '') {
    window.location.href="index.php";
  }
</script>

<section class="player-slider" id="player-slider">
  <div class="gradient-overlay"></div>
  <div class="container" id="player-info">
    <div class="player-bg">
      <h1 id="title-player-name"></h1>
      <h1 class="squad-no" id="title-squad-no"></h1>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content" id="single-player">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-12 left">
        <div class="box">
          <div class="image">
            <img src="" id="player-image">
          </div>
          <div class="box-content">
            <div class="title">
              Player Profile
            </div>
            <table class="table">
              <tr>
                <td>Name:</td>
                <td id="name"></td>
              </tr>
              <tr>
                <td>Squad No:</td>
                <td id="squad_no"></td>
              </tr>
              <tr>
                <td>Current Club:</td>
                <td id='team'></td>
              </tr>
              <tr>
                <td>Position:</td>
                <td id='position'></td>
              </tr>
              <tr>
                <td>Born:</td>
                <td id="dob"></td>
              </tr>
              <tr>
                <td>Age:</td>
                <td id="age"></td>
              </tr>
            </table>
          </div>
        </div><!--box -->
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12 right">
        <div class="player-details" id="desc">
        </div>
      </div>
    </div><!-- row-->
  </div><!-- container -->
</section><!-- content -->

<?php include('footer.php'); ?>
<script src="scripts/single-player.js" charset="utf-8"></script>
