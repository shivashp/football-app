// var basepath = "/greenacre/api/api.php?choice=";
// If this doesn't work

var basepath = "http://localhost:8888/greenacre/api/api.php?choice=";


function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}


var FUNC = {
    search: function(query, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "search-news",
        dataType: "json",
        data: {query},
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    contact: function(data, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "contact",
        dataType: "json",
        data: data,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getAllNews: function(callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-news",
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });// Ajax
    },// Get all News
    getSingleNews: function(slug, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-single-news",
        data: {
          "slug": slug
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });// Ajax
    },
    deleteNews: function(id, token, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "delete-news",
        data: {
          "id": id,
          "access_token": token
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    addNews: function(formData, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "add-news",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formData,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getAllTeams: function(callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-teams",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
          showError(data.message);
        }
      });// Ajax
    },// Get all Teams
    addTeam: function(formData, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "add-team",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formData,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    deleteTeam: function(id, token, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "delete-team",
        data: {
          "id": id,
          "access_token": token
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    addPlayer: function(formData, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "add-player",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formData,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    deletePlayer: function(id, token, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "delete-player",
        data: {
          "id": id,
          "access_token": token
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getAllPlayers: function(callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-all-players",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
          showError(err.message);
        }
      });// Ajax
    },// Get all Teams
    getPlayersByTeam: function(id, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-player-by-team",
        data: {
          "id": id
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getSinglePlayer: function(id, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-single-player",
        data: {
          "id": id
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getAllFixtures: function(callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-fixtures",
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });// Ajax
    },// Get all News
    addFixture: function(data, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "create-fixture",
        dataType: "json",
        data: data,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    deleteFixture: function(id, token, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "delete-fixture",
        data: {
          "id": id,
          "access_token": token
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getSingleFixture: function(id, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-single-fixture",
        data: {
          "id": id
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    updateResult: function(data, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "update-result",
        dataType: "json",
        data: data,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    registerUser: function(formData, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "register-user",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formData,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    loginUser: function(data, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "login-user",
        dataType: "json",
        data: data,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    chargeUser: function(data, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "charge-user",
        dataType: "json",
        data: data,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    subscribe: function(email, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "subscribe",
        dataType: "json",
        data: {email},
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getSingleUser: function(id, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-single-user",
        data: {
          "id": id
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    getUserTickets: function(id, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "view-user-tickets",
        data: {
          "id": id
        },
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },
    updateAdminPassword: function(data, callback) {
      $.ajax({
        type: "POST",
        url: basepath + "admin-update-password",
        dataType: "json",
        data: data,
        success: function(data) {
          if(data.status) {
            callback(null, data);
          } else {
            callback(data.message, null);
            showError(data.message);
          }
        },
        error: function(err) {
          callback(err.message, null);
        }
      });
    },


}// FUNC


getTeams();

function getTeams() {
  var team_list = null;
  if(team_list !== null && team_list !== undefined && team_list !== '') {
    team_list = JSON.parse(team_list);
    renderTeams(team_list);
    return false;
  }
  FUNC.getAllTeams(function(err, data) {
    if(!err) {
      sessionStorage.setItem('team_list', JSON.stringify(data.data))
      renderTeams(data.data)
    }
  })
}

function renderTeams(teams) {
  $("#team-dropdown").html('');
  if(teams.length == 0){
    return false;
  }
  teams.map( function (team) {
    var str="";
    str += "<li><a href=\"single-team.php?name="+team.name+"&&id="+team.team_id+"\">"+team.name+"<\/a><\/li>";
    $("#team-dropdown").append(str);
  });
}

var user_access_token = localStorage.getItem('access_token');
var name = localStorage.getItem('name');

if(name !== null && user_access_token !== null) {
  var str="";
  str += "<a href=\"logout.php\"> &nbsp;Logout<\/a>";
  $(".membership>span").html(str);
  $(".user-profile").css("display", "block")
}
