function startCounter(date, element) {
  // Set the date we're counting down to
  var countDownDate = new Date(date).getTime();

  // Update the count down every 1 second
  this.x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById(element).innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(this.x);
      document.getElementById(element).innerHTML = "EXPIRED";
    }
  }, 1000);
}

// Checks if the string is blank
String.prototype.isBlank = function (type = 'Field') {
  var value = this.toString();
  value = value.trim();
  if (value == '' || (/^\s*$/.test(value))) {
    showError(type + " Cannot Be Empty");
    return false;
  }
  else
  return true;
};

// Checks if the string is equal to another string
String.prototype.equals = function (value2, type = 'Data') {
  var value1 = this.toString();
  if (value1 != value2) {
    showError(type + " Do Not Match");
    return false;
  }
  else
  return true;
};

// Checks if the string is valid email
String.prototype.isEmail = function (type = 'Email') {
  var value = this.toString();
  var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
  if (!(testEmail.test(value))) {
    showError(type + " Should Be Valid");
    return false;
  }
  else
  return true;
};
var isNull = function(value, type = 'Field') {
  if(value === null) {
    showError(" Please Choose " + type);
    return false;
  }
  return true;
}

function showError(message) {
  $.notify({
      icon: "notifications",
      message: message

    },{
        type: 'danger',
        timer: 500,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
}

function showSuccess(message) {
  $.notify({
      icon: "notifications",
      message: message

    },{
        type: 'success',
        timer: 500,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
}

function checkEmpty(arr){
	arr = arr.filter(data => {return !data})
	if(arr.length > 0){
		return true;
	}
	return false;
}

$(".header-search").click(function(e) {
  e.preventDefault();
  var query = $("#header-search-query").val();
  // var url = window.location.pathname.split('/');
  // var page = url[url.length-1];  
  window.location.href = 'search.php?query='+query;

})
