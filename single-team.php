<?php include('header.php'); ?>
<script>
  var team_id = "<?php echo $_GET['id']; ?>";
  var team_name = "<?php echo $_GET['name']; ?>";
  if(team_id == null || team_id == undefined || team_name == null || team_name == undefined || team_name == '' || team_id == '') {
    window.location.href="index.php";
  }
</script>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1 class="team-name"></h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li>Team</li>
        <li class="active team-name"></li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content" id="single-team">
  <div class="container">

    <div class="players">

      <div class="section-title">
        GOALKEPPERS
      </div>
      <div class="section-body row" id="goalkeepers">
      
      </div><!-- section-body-->

      <div class="section-title">
        DEFENDERS
      </div>
      <div class="section-body row" id="defenders">
      </div><!-- section-body-->

      <div class="section-title">
        MIDFIELDERS
      </div>
      <div class="section-body row" id="midfielders">
      </div><!-- section-body-->

      <div class="section-title">
        FORWARDS
      </div>
      <div class="section-body row" id="forwards">
      </div><!-- section-body-->

    </div><!-- players -->
  </div><!-- container -->
</section><!-- single-team -->

<?php include('footer.php'); ?>
<script src="scripts/single-team.js" charset="utf-8"></script>
