<?php include('header.php'); ?>

<div id="profile-banner">
  <div class="dark-overlay"></div>
</div><!-- profile-banner -->

<div class="row" id="user-section">
  <div id="user-image">
      <img src="">
  </div><!--user-image -->
  <div id="user-name">
      <h1 id="name"></h1>
      <p id="profile-address"></p>
  </div><!-- user-name -->
</div><!--user-section-->

<div class="content" id="user-profile">
  <div class="container">
    <div class="section-title">
      <h1>My Tickets</h1>
    </div><!--section-title -->

    <div class="section-body">
      <table class="table">
        <thead>
          <tr>
            <th>Date</th>
            <th>Time</th>
            <th>Match</th>
            <th>Location</th>
            <th>Price</th>
            <th>Status</th>
            <th>Ticket</th>
          </tr>
        </thead>
        <tbody id="table-body">

        </tbody>
      </table>
    </div>

  </div><!--container-->
</div><!-- content-->


<div id="printableArea">
  <!--top section  -->
  <div style="width:750px;height:248px;background:#f9f1f1;font-family: sans-serif;    position: relative;">
    <div style="text-align:center;height:100px;background: url(https://s3.envato.com/files/88880472/Image_Preview.jpg) no-repeat center center;background-size: cover;    border: 1px solid #ccc;">
      <div style="  background-image: linear-gradient(to right, #9e8383 0%, #637379 100%);
      opacity: 0.9;
      height: 100px;">

    </div>
    <h2 style="letter-spacing: 10px;
    padding-top: 7px;
    z-index: 50;
    position: absolute;
    top: -8px;
    color: white;
    width: 100%;">THE <br> PERFECT MATCH</h2>
    <!-- header-end -->
    <!-- flex-container- -->
    <div style="opacity: 0.3;"> </div>
    <div style="display:flex;height:215px;background-image: linear-gradient(-20deg, #e9defa 0%, #fbfcdb 100%);
    background-size: cover;    border: 1px solid #ccc;
    border-top: none;
    ">
    <div  style="width:500px;">
      <h1 style="margin-top: 24px;margin-bottom: 0px;" id="print-team-names">Real Madrid <span>vs</span> Barcelona</h1>
      <h2 style="margin-top: 13px;margin-bottom: 0px;" id="print-team-date">Sunday 20 September 2017 </h2>
      <h3 style="margin-top: 13px;margin-bottom: 0px;" id="print-team-location">Santiago Bernabéu Stadium</h3>
      <h2 style="margin-top: 13px;margin-bottom: 0px;">KICK OFF <span id="print-team-time">4:00 PM</span></h2>
    </div>
    <!-- left -->
    <div style="width:250px;border-left: 2px dashed #9a9494;">
      <h1 style="font-size:27px;margin-top:24px;" id="print-user-name"></h1>
      <h3 style="">Price</h3>
      <p style="font-size: 65px;
      margin: 0px;
      font-style: italic;" id="print-price">$70</p>

    </div>
  </div>
  <!-- right start -->
</div>

<!-- flex-container- -->

</div><!--header  -->
<div>
  <p style="box-sizing: border-box;
  margin-top: 68px;
  background-image: linear-gradient(to right, #9e8383 0%, #637379 100%);
  padding: 20px;
  width: 750px;
  border: 1px solid #ccc;
border-top: none;
  text-align: center;color:white;">PLEASE TAKE YOUR SEATS 30 MINUTES BEFORE THE START OF THE MATCH</p>

</div>
</div><!--container  -->
</div>

<?php include('footer.php'); ?>
<style media="screen">
@media print {
body {-webkit-print-color-adjust: exact;}
}
  #printableArea {
   display: none;
  }
  #print-user-name {
    text-transform: capitalize;
  }
  .section-title {
    margin-top: 0;
    margin-bottom:20px;
    text-align: left;
    padding-bottom: 10px;
  }
  th {
    color: #2C3544;
    font-size: 15px;
  }
</style>
<script src="scripts/user-profile.js" charset="utf-8"></script>
