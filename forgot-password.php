<?php include('header.php'); ?>

<section class="content login-register" id="forgot-password">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
        <div class="box">
          <div class="header">
            Recover Password
          </div><!-- header -->          
          <div class="body">
            <label>Email:</label>
            <input type="text" id="email" placeholder="Email" class="form-control">
            <div class="btn-section">
              <button type="button" class="btn btn-primary" name="button">Reset Password</button>
            </div>
            <div class="account-msg">
              Go back to <a href="login.php">Login</a>
            </div>
          </div><!-- body -->
        </div><!-- box -->
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- login -->

<?php include('footer.php'); ?>
