<?php include('header.php'); ?>

<script>
  var id = '<?php echo $_GET['id']; ?>';
  if(id == null || id == undefined || id == '') {
    window.location.href="tickets.php";
  }
</script>

<div class="content" id="tickets-page">
    <div class="container">
      <div class="ticket">
         <div class="col-md-3 col-xs-12 pull-right ticket-details">
            <div class="timings">
               <div class="ticketlabel">Price:</div>
               <span id="t-price"></span>
            </div>
            <div class="timings">
               <div class="ticketlabel">Date/Time:</div>
               <span id="t-time"></span>
            </div>
            <div class="venue">
               <div class="ticketlabel">Location: </div>
               <span id="t-location"></span>
            </div>
         </div>
         <div class="col-md-9 col-xs-12">
            <div class="title" id="team-title">
               <!-- <img src="img/teams/b1bfcaf55a8ddaf64e084653af63499dfreakoids-logo.png"> Real Madrid <span class="vs">Vs</span> Barcelona <img src="img/teams/b1bfcaf55a8ddaf64e084653af63499dfreakoids-logo.png"> -->
            </div>
         </div>
      </div><!-- ticket-->

      <div class="buy-button col-md-9">
        <button type="button" data-toggle="modal" data-target="#card-payment" class="btn btn-primary buyTicket" name="button">Pay with Card</button>
        <!-- <button type="button" class="btn btn-primary buyTicket" data-id="14" name="button">Pay with Paypal</button> -->
      </div>


    </div><!-- container-->
</div><!-- buy-ticket-->

<div id="card-payment" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pay with Card</h4>
      </div>
      <div class="modal-body">
        <span class="payment-errors"></span>
        <span class="payment-success"></span><br>
        <div class='card-wrapper'></div>
        <form id="payment-form">
            <input type="text" class="form-control" name="name" placeholder="Name"/>
            <input type="text" class="form-control" data-stripe="number" name="number" placeholder="Card Number">
            <div class="row">
              <div class="col-xs-3">
                <input type="text" name="expiry" data-stripe="exp-month" class="form-control" placeholder="MM"/>
              </div>
              <div class="col-xs-3">
                <input type="text" name="expiry" data-stripe="exp-year" class="form-control" placeholder="YY"/>
              </div>
              <div class="col-xs-6">
                <input type="text" data-stripe="cvc" name="cvc" class="form-control" placeholder="CVC"/>
              </div>
            </div><!-- row-->
      </div><!-- modal-body-->
      <div class="modal-footer">
        <button type="submit" id="pay" class="btn btn-primary">Pay</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>

  </div>
</div>

<style media="screen">
  .card-wrapper {
      margin-top: 30px;
  }
  .buy-button {
    text-align: center;
    margin-top: 50px;
    margin-bottom: 50px;
  }
  .buyTicket {
    margin-right: 10px;
    margin-left: 20px;
    padding: 11px 45px;
    font-size: 18px;
    border-radius: 5px;
  }
  #tickets-page .ticket .title {
    padding: 74px 0px;
  }
  .payment-success {
    color: green;
  }
  .payment-errors {
    color: red;
  }
</style>
<?php include('footer.php'); ?>
<script src="js/jquery.card.js" charset="utf-8"></script>
<script src="https://js.stripe.com/v2/"></script>
<script src="scripts/buy-ticket.js" charset="utf-8"></script>
