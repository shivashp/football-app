<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Greenarce Eagles</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>

    <section id="top-bar">
      <div class="container">
        <div class="address">
          <i class="fa fa-map-marker"></i> &nbsp;
          010 Avenue of the Mon. sydney Autralia <span class="hd-smi"><a href="#"><i class="fa fa-facebook-square"></i></a> <a href="#"><i class="fa fa-instagram"></i></a><a href="#"><i class="fa fa-youtube-play"></i></a></span>
        </div>
        <div class="membership">
          <span>Membership <a href="login.php">&nbsp;Login</a> &nbsp;&amp;&nbsp; <a href="register.php">Register</a></span>
        </div><!-- membership -->
      </div><!-- container -->
    </section><!--top-bar -->

    <section id="header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <div id="logo">
              <a href="index.php"><img src="img/logo.png"></a>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-hidden">
            <form>
              <div class="input-group search">
                <input type="text" class="form-control" id="header-search-query" placeholder="Search from here">
                <input type="submit" hidden class="header-search">
                <span class="input-group-addon header-search"><i class="fa fa-search"></i></span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section><!-- header -->

    <section id="navgation-bar">
      <nav class="navbar navbar-default navbar-green">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="index.php">Home</a></li>
              <li><a href="user-profile.php" class="user-profile">My Profile</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fixtures &amp; Result <span class="fa fa-angle-down"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="fixture.php">Fixture</a></li>
                  <li><a href="result.php">Result</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Teams <span class="fa fa-angle-down"></span></a>
                <ul class="dropdown-menu" id="team-dropdown">
                </ul>
              </li>
              <li><a href="gallery.php">Gallery</a></li>
              <li><a href="news.php">News &amp; Events</a></li>
              <li><a href="tickets.php">Tickets</a></li>
              <li><a href="contact.php">Contact</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-->
      </nav>
    </section><!-- navigation-bar -->
