<script>
    var query = '<?php echo $_GET['query']; ?>';
    if(query == '') {
      window.history.back();
    }
</script>
<?php include('header.php'); ?>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>SEARCH RESULTS</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Search</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->


<section class="content" id="news-events">
  <div class="container">
    <div class="section-title" style="
    margin-top: 0;
    margin-bottom: 35px;
    border-bottom: 1px solid #ccc;
    width: 100%;
    text-align: left;
    padding-bottom: 20px;">
      Search Results...
    </div>
    <div id="news-body">

    </div>
    <div id="news-body-pagination" style="display:none">
        <a id="news-body-previous" href="#" class="disabled btn btn-primary btn-raised pull-left">&laquo; Previous</a>
        <a id="news-body-next" href="#" class="btn btn-primary btn-raised pull-right">Next &raquo;</a>
    </div>
  </div><!-- container-->
</section>


<?php include('footer.php'); ?>
<script src="js/jquery.paginate.min.js" charset="utf-8"></script>

<script>
    getSearchResults(query);
    function getSearchResults(query) {
      FUNC.search(query, function(err, data) {
          if(!err) {
            $("#news-body").html('');
            if(data.data.length == 0) {
              noNews();
              return false;
            }
            data.data.map(function(news) {
              var str="";
              var desc = (news.description.length > 300)?news.description.substr(0,300)+'...':news.description;
              desc = desc.replace(/<(?:.|\n)*?>/gm, '');
              var created_at = moment(news.created_at).fromNow();
              str += "<div class=\"news\">";
              str += "          <div class=\"image\">";
              str += "            <img src=\""+news.featured_image+"\">";
              str += "          <\/div>";
              str += "          <div class=\"title\">";
              str += "            <a href=\"single-news.php?news="+news.slug+"\">"+news.title+"<\/a>";
              str += "          <\/div>";
              str += "          <div class=\"meta\">";
              str += "            by Admin | "+created_at;
              str += "          <\/div>";
              str += "          <div class=\"body\">";
              str += "            <p>"+desc+"<\/p>";
              str += "          <\/div>";
              str += "        <\/div>";

              $("#news-body").append(str);
            });
            $('#news-body').paginate({itemsPerPage: 3});
          }
      });
    }

    function noNews() {
        var str="";
        str += "  <div class=\"no-data\">";
        str += "          No news found for your search";
        str += "        <\/div>";
        $("#news-body").html(str);
    }
</script>
