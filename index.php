<?php include('header.php'); ?>

<section id="slider">
  <div class="dark-overlay"></div>
  <div class="text-section">
    <div class="container">
      <div class="col-md-7 col-sm-9 col-xs-12">
        <h1>Welcome to Greenacre Eagles</h1>
        <p>Bushwick small batch ethical pitchfork irony cred wolf subway tile lomo locavore neutra. 90's venmo poke thundercats mustache.</p>
        <div class="button-section">
          <button class="btn btn-primary btn-lg">READ MORE</button>
          <button class="btn btn-dark btn-lg">WATCH VIDEO</button>
        </div><!-- button-section-->
      </div>
    </div><!-- container -->
  </div><!--text-section-->
</section><!-- slider -->

<section class="content" id="landing-content">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-sm-12">
        <div id="newsGroup">
          <div class="widget-box">
            <div class="widget-header">
              LATEST NEWS
            </div>
            <div class="widget-body">
              <div id="news-body">

              </div>
              <div class="row" id="news-body1">
              </div><!-- row -->

            </div><!-- widget-body -->
          </div><!-- widget-box -->
        </div><!-- newsGroup -->
        <div id="fixtures">
          <div class="widget-box">
            <div class="widget-header">UPCOMING FIXTURE</div>
            <div class="widget-body" id="fixture-body">

            </div><!-- widget-body-->
          </div><!-- widget-box -->
        </div><!-- fixture -->
        <div id="gallery">
          <div class="widget-box">
            <div class="widget-header">
              MATCH GALLERY <span class="small pull-right"><a href="gallery.php">(VIEW ALL)</a></span>
            </div>
            <div class="widget-body">
              <div><img src="img/gallery/1.jpg" alt=""></div>
              <div><img src="img/gallery/2.jpg" alt=""></div>
              <div><img src="img/gallery/3.jpg" alt=""></div>
              <div><img src="img/gallery/4.jpg" alt=""></div>
              <div><img src="img/gallery/5.jpg" alt=""></div>
              <div><img src="img/gallery/6.jpg" alt=""></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-sm-12 sidebar-section">
        <div id="matches">
          <div class="widget-box">
            <div class="widget-header text-center">
              <i class="fa fa-angle-left" id="fixture-go-left"></i> &nbsp; NEXT MATCHES &nbsp; <i class="fa fa-angle-right" id="fixture-go-right"></i>
            </div>
            <div class="widget-body">
              <div class="side-widget" id="single-fixture-slider">
                <div id="sb-fixture-data">
                </div><!-- Temp Div -->
              </div><!-- side-widget-->
            </div><!-- widget-body -->
          </div><!-- widget-box -->
        </div><!-- matches -->
        <div id="tickets">
          <div class="widget-box">
            <div class="widget-header text-center">
              <i class="fa fa-angle-left" id="ticket-go-left"></i> &nbsp; BUY TICKETS &nbsp; <i class="fa fa-angle-right" id="ticket-go-right"></i>
            </div>
            <div class="widget-body">
              <div class="side-widget" id="sb-ticket-slider">
                <div id="sb-ticket-body">

                </div><!-- ticket-body-->
              </div><!--side-widget-->
            </div><!-- widget-body -->
          </div><!-- widget-box -->
        </div><!-- tickets -->
        <div class="ads">
          <img src="img/gallery/car-advert.png">
        </div>
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- Landing-content -->

<section id="subscribe">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-6 col-xs-12">
        <div class="left-message">
          Are you a Greenacre Eagles? Subscribe to the newsletter and keep up to date with all the latest news!
        </div>
      </div>
      <div class="col-md-5 col-sm-6 col-xs-12">
        <form>
          <div class="input-group search">
            <input type="text" class="form-control" id="subs-email" placeholder="Your email address">
            <input type="submit" class="subscribe-btn" hidden>
            <a href="javascript:;" id="subscribe-btn" class="input-group-addon btn-dark subscribe-btn">SUBSCRIBE</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<?php include('footer.php'); ?>
<script src="http://pioul.fr/ext/jquery-diyslider/jquery.diyslider.min.js" charset="utf-8"></script>
<script src="scripts/index.js" charset="utf-8"></script>
<script>
  //startCounter();
</script>
