<?php include('header.php'); ?>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>NEWS &amp; EVENTS</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">News &amp; Events</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content" id="news-events">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7 col-xs-12 left">
        <div id="news-body">

        </div>
        <div id="news-body-pagination" style="display:none">
            <a id="news-body-previous" href="#" class="disabled btn btn-primary btn-raised pull-left">&laquo; Previous</a>
            <a id="news-body-next" href="#" class="btn btn-primary btn-raised pull-right">Next &raquo;</a>
        </div>
      </div><!-- left -->
      <div class="col-md-4 col-sm-6 col-sm-12 sidebar-section">
        <div id="matches">
          <div class="widget-box">
            <div class="widget-header text-center">
              <i class="fa fa-angle-left" id="fixture-go-left"></i> &nbsp; NEXT MATCHES &nbsp; <i class="fa fa-angle-right" id="fixture-go-right"></i>
            </div>
            <div class="widget-body">
              <div class="side-widget" id="single-fixture-slider">
                <div id="sb-fixture-data">
                </div><!-- Temp Div -->
              </div><!-- side-widget-->
            </div><!-- widget-body -->
          </div><!-- widget-box -->
        </div><!-- matches -->
        <div id="tickets">
          <div class="widget-box">
            <div class="widget-header text-center">
              <i class="fa fa-angle-left" id="ticket-go-left"></i> &nbsp; BUY TICKETS &nbsp; <i class="fa fa-angle-right" id="ticket-go-right"></i>
            </div>
            <div class="widget-body">
              <div class="side-widget" id="sb-ticket-slider">
                <div id="sb-ticket-body">

                </div><!-- ticket-body-->
              </div><!--side-widget-->
            </div><!-- widget-body -->
          </div><!-- widget-box -->
        </div><!-- tickets -->
        <div id="results">
          <div class="widget-box">
            <div class="widget-header text-center">
              LATEST RESULT <span class="small">(ON LIVE)</span>
            </div>
            <div class="widget-body">
              <div class="side-widget">
                <div class="match">
                  <div class="team">
                    <div>Team A</div>
                    <img src="img/team2.png">
                    <div class="score loss">
                      <div class="score-title">LOSS</div>
                      <div class="score-number">2</div>
                    </div>
                  </div>
                  <div class="team">
                    <div>Team B</div>
                    <img src="img/team1.png">
                    <div class="score won">
                      <div class="score-title">WON</div>
                      <div class="score-number">2</div>
                    </div>
                  </div><!-- team -->
                </div><!-- match -->
                <div class="date-time">
                  08 OCTOBER 2017 22:00 (FT)
                </div>
              </div><!-- side-widget -->
            </div><!-- widget-body -->
          </div><!-- widget-box -->
        </div><!-- tickets -->
        <div class="ads">
          <img src="img/gallery/car-advert.png">
        </div>
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- news-events -->

<?php include('footer.php'); ?>
<script src="http://pioul.fr/ext/jquery-diyslider/jquery.diyslider.min.js" charset="utf-8"></script>
<script src="js/jquery.paginate.min.js" charset="utf-8"></script>
<script src="scripts/news.js" charset="utf-8"></script>
