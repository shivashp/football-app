<?php include('header.php'); ?>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>CONTACT US</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Contact us</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section class="content" id="contact-page">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3316.377670290666!2d151.14313431559415!3d-33.776744121719034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a8a6bc4c14d3%3A0xbac9a2f6846e2bed!2s10+Montreal+Ave%2C+Killara+NSW+2071%2C+Australia!5e0!3m2!1sen!2snp!4v1504449615910" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="contact-info"><span class="fa fa-map"></span> 010 Avenue of the Mon. sydney Autralia</div>
        <div class="contact-info"><span class="fa fa-phone"></span> 123 456 78924</div>
        <div class="contact-info"><span class="fa fa-envelope"></span> info@greenacreeagles.com</div>
        <form>
          <input type="text" id="name" class="form-control" placeholder="Name">
          <input type="text" id="email" class="form-control" placeholder="Email">
          <textarea class="form-control" id="message" rows="8" cols="80"></textarea>
          <button type="submit" id="send-message" class="btn btn-primary btn-lg">SEND MESSAGE</button>
        </form>

      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- content -->

<?php include('footer.php'); ?>
<script src="scripts/contact.js"></script>
