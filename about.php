<?php include('header.php'); ?>

<section class="inner-slider">
  <div class="dark-overlay"></div>
  <div class="container">
    <h1>ABOUT US</h1>
  </div>
  <div class="bread-crumb-row">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">About us</li>
      </ol>
    </div>
  </div>
</section><!-- inner-slider -->

<section id="about-page" class="content">
  <div class="container">
    <div class="row" id="top-row">
      <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
        <img src="img/about.jpg">
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <h2 class="about-title">WHO WE ARE</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </div><!-- top-row -->
    <div id="facilities">
      <div class="section-title">
        CLUB FACILITIES
      </div>
      <p class="section-subtitle">Kings Club Despicable briefly jeepers much roughly sped ouch in one away supportive grateful.</p>
      <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon">
              <img src="img/facilities/users.png">
            </div>
            <div class="title">
              PLAYER WORKSHOPS
            </div>
            <p class="body">
              Lorem ipsum dolor sit amet incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            </p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon">
              <img src="img/facilities/gallery.png">
            </div>
            <div class="title">
              MEDIA GALLERIES
            </div>
            <p class="body">
              Lorem ipsum dolor sit amet incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            </p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon">
              <img src="img/facilities/ladies.png">
            </div>
            <div class="title">
              LADIES TEAM
            </div>
            <p class="body">
              Lorem ipsum dolor sit amet incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            </p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon">
              <img src="img/facilities/soccer-board.png">
            </div>
            <div class="title">
              SOCCER BOARD
            </div>
            <p class="body">
              Lorem ipsum dolor sit amet incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            </p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon">
              <img src="img/facilities/coach.png">
            </div>
            <div class="title">
              DEDICATED COACHS
            </div>
            <p class="body">
              Lorem ipsum dolor sit amet incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            </p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="icon">
              <img src="img/facilities/tournament.png">
            </div>
            <div class="title">
              TOURNAMENT
            </div>
            <p class="body">
              Lorem ipsum dolor sit amet incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            </p>
        </div>


      </div><!-- row -->
    </div><!-- facilities -->
    <div id="team">
      <div class="section-title">
        OUR TEAM
      </div>
      <div class="row">

        <div class="col-md-3 col-sm-6 col-xs-12 single-person">
          <div class="photo">
            <img src="http://freakoids.com/images/shiva.jpg">
          </div>
          <div class="name">
            Shiva Pandey
          </div>
          <div class="position">
            Chief Executive Officer
          </div>
          <div class="desc">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </div>
        </div><!-- single-person -->

        <div class="col-md-3 col-sm-6 col-xs-12 single-person">
          <div class="photo">
            <img src="http://freakoids.com/images/robus.jpg">
          </div>
          <div class="name">
            Robus Gauli
          </div>
          <div class="position">
            Chief Technology Officer
          </div>
          <div class="desc">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </div>
        </div><!-- single-person -->

        <div class="col-md-3 col-sm-6 col-xs-12 single-person">
          <div class="photo">
            <img src="https://pbs.twimg.com/profile_images/872957497086291969/1bpxCxsq_400x400.jpg">
          </div>
          <div class="name">
            John Smith
          </div>
          <div class="position">
            Chief Marketing Officer
          </div>
          <div class="desc">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </div>
        </div><!-- single-person -->

        <div class="col-md-3 col-sm-6 col-xs-12 single-person">
          <div class="photo">
            <img src="http://en.widenews24.com/wp-content/uploads/2016/12/zakarburgh.jpg">
          </div>
          <div class="name">
            Mark Zuckerberg
          </div>
          <div class="position">
            Chief Financial Officer
          </div>
          <div class="desc">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </div>
        </div><!-- single-person -->


      </div><!-- row -->
    </div><!-- team -->
  </div><!-- container -->
</section><!-- about-page-->

<?php include('footer.php'); ?>
